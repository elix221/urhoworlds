My game development experiments with  [Urho3D](https://urho3d.github.io)

As of 2020 january 16 there are:

* Procedurally generated (and therefore infinitely big in theory) landscape
  
  Under the hood [FastNoise](https://github.com/Auburns/FastNoise) library is
  used for landsape height map
  
* Procedurally generated trees

* Impostors system using technique 
[described](https://www.shaderbits.com/blog/octahedral-impostors) 
at shaderbits.com

TODO some proper screenshots.

Old video of the impostor system in action: 
https://www.youtube.com/watch?v=1cROTxRlN6s&t=86s
