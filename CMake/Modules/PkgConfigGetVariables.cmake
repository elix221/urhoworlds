macro (cmake_pkgconfig_get_vars P_VARNAME_PREFIX P_PKG_CONFIG_FILENAME)

  file(STRINGS ${P_PKG_CONFIG_FILENAME} S)

  foreach(L ${S})
    string(FIND "${L}" "=" N)
    if (${N} GREATER 1)
      math(EXPR NPP "${N} + 1")
      string(SUBSTRING ${L} ${NPP} -1 MY_VAL)
      string(SUBSTRING ${L} 0 ${N} MY_VAR)

      set("${P_VARNAME_PREFIX}_${MY_VAR}" ${MY_VAL})
    endif()

    string(FIND "${L}" ":" N)
    string(LENGTH "${L}" SZ)
    if (${N} GREATER 1)
      math(EXPR NPP "${N} + 2")
      if (${NPP} LESS ${SZ})
        string(SUBSTRING ${L} ${NPP} -1 MY_VAL)
        string(SUBSTRING ${L} 0 ${N} MY_VAR)
        set("${P_VARNAME_PREFIX}_${MY_VAR}" ${MY_VAL})
      endif()
    endif()
  endforeach()
endmacro()

