#ifndef RACINGGAME_SETTINGS_HPP__
#define RACINGGAME_SETTINGS_HPP__

#include <map>
#include <string>

#include "Setting.hpp"

typedef std::map<std::string, Setting> SettingsMap;
typedef std::map<std::string, Setting*> SettingsPtrsMap;
typedef std::map<std::string, SettingsPtrsMap> SettingsCategories;

class SettingsIterator {
public:
    SettingsIterator (SettingsPtrsMap::iterator b,
                      SettingsPtrsMap::iterator e);

    void peekNext ();
    bool isElement () const;
    Setting& getCurrent () const;

private:
    const SettingsPtrsMap::iterator begin;
    const SettingsPtrsMap::iterator end;
    SettingsPtrsMap::iterator current;
    bool currentIsElement;
};

class SettingsCategoriesIterator {
public:
    SettingsCategoriesIterator (SettingsCategories::iterator b,
                                SettingsCategories::iterator e);
    void peekNext ();
    bool isElement () const;
    std::string getName ();
    SettingsIterator getSettingsIterator ();
private:
    const SettingsCategories::iterator begin;
    const SettingsCategories::iterator end;
    SettingsCategories::iterator current;
    bool currentIsElement;
};


// usage: 1) create instance with (optional) filename
// 2) add settings by calling .appendSetting
// call .load() to load setting values from file
class Settings {
public:
    explicit Settings (const std::string& filename = "");
    ~Settings ();
    Setting& appendSetting (Setting set);
    void load ();
    void setFilename (const std::string& name) {
        this->filename = name;
    }
    template<typename T> T get (const std::string& name) const {
        const T fake{};
        return getSetting(name).getValue(fake);
    }
    void loadFromFile (const std::string &pFileName);
    void reloadFromFile ();
    const Setting& getSetting (const std::string& name) const;
    Setting& getSetting (const std::string& name);
    bool hasSetting (const std::string& name) const;
    SettingsIterator getSettingsIterator ();
    SettingsIterator  getCategorySettingsIterator (const std::string& cat_name);
    SettingsCategoriesIterator getSettingsCategoriesIterator ();
    void writeFile ();
private:

    void loadSettings ();

    SettingsMap values;
    SettingsPtrsMap valuesMap;
    SettingsCategories categories;
    std::string filename;
};

#endif // RACINGGAME_SETTINGS_HPP__
