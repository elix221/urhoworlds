#pragma once


#include <memory>
#include <vector>
#include <unordered_map>


#include <Urho3D/Math/Vector3.h>
#include <Urho3D/Scene/Node.h>
#include <Urho3D/Math/Vector2.h>
#include <Urho3D/Core/Object.h>
#include <Urho3D/Core/Context.h>
#include <Urho3D/Container/Str.h>
#include <Urho3D/Container/HashMap.h>
#include <Urho3D/Math/StringHash.h>
#include <Urho3D/Math/Color.h>
#include <Urho3D/UI/UIElement.h>


#include "UrhoBulletIncludeFix.hpp"
#include "Settings/Settings.hpp"
#include "LandscapeGenerator.hpp"
#include "LandscapeManager.hpp"
#include "Impostor.hpp"


struct UrhoworldsPhysicsData {
    UrhoworldsMemory<UrhoworldsIndexElement> *index_data;
    UrhoworldsMemory<Urho3D::Vector3> vertex_data;
    btTriangleIndexVertexArray bt_mesh;
    std::unique_ptr<btBvhTriangleMeshShape> bt_shape;
};

struct MapTileData {
    std::unique_ptr<UrhoworldsPhysicsData> physics;
    Urho3D::Node* node = nullptr;
};


struct ModelLoadWorkerData {
	Noizes *  nz = nullptr;
    std::unique_ptr<UrhoworldsPhysicsData> physics_data;
    UrhoworldsPlaneConstructionData<UrhoworldsLandscapeVertexElement> lod0;
    UrhoworldsPlaneConstructionData<UrhoworldsLandscapeVertexElement> lod1;
    Urho3D::Vector3 position;
    Urho3D::Vector3 bounds0;
    Urho3D::Vector3 bounds1;

    std::vector<Urho3D::Matrix3x4> trees;

    unsigned short size = 0;
    unsigned short resolution = 0;
    Urho3D::IntVector2 pos;

    bool cancel = false;
};

class Landscape : public Urho3D::Object {
public:
    URHO3D_OBJECT(Landscape, Urho3D::Object);

    explicit Landscape (Urho3D::Context *pContext);
    void init (Settings *pSettings,
               Urho3D::Node *pCameraNode);
    void initSettings ();
    void landscapeSettingUpdated ();
    void debugDrawSettingUpdated ();
    void showDbgMapSettingUpdated ();
    void displayTextOnNode (Urho3D::Node *pNode,
                            const Urho3D::String &pText);

    bool isOutsideDistance (const Urho3D::IntVector2 &pos_2d,
                            const Urho3D::IntVector2 &pos_i,
                            const unsigned distance);

    Urho3D::IntVector2 getCameraTilewisePosition (const Urho3D::Vector3 &pPos);

    void processUpdateEvent (Urho3D::StringHash eventType,
                             Urho3D::VariantMap& eventData);

    void removeTilesOutsideRange ();

    void updateCurrentWorksTiles ();

    void startJobsForNeededTiles ();

    void dbgTileUpdate (const Urho3D::IntVector2 &pTilePos,
                        const Urho3D::IntVector2 &pCamPos2d,
                        const Urho3D::Color &pColor,
                        const float pOpacity);

    void dbgOuterTileUpdate (const Urho3D::IntVector2 &pTilePos,
                        const Urho3D::IntVector2 &pCamPos2d,
                        const Urho3D::Color &pColor,
                        const float pOpacity);

    void __dbgTileUpdate (const unsigned pResolution,
                          const Urho3D::IntVector2 &pTilePos,
                          const Urho3D::IntVector2 &pCamPos2d,
                          const Urho3D::Color &pColor,
                          const float pOpacity);

    void dbgRemoveElement(const Urho3D::IntVector2 &pPos,
                          const unsigned pResolution);

    bool __isOutsideDistance(const Urho3D::IntVector2& pCamPos,
                             const Urho3D::IntVector2& pPos,
                             const unsigned pResolution);

    void createWorkItem (const Urho3D::IntVector2& pos_i,
                         const unsigned pSize,
                         const unsigned pResolution,
                         const unsigned priority);

    void processWorkItemCompleted (Urho3D::StringHash eventType,
                                   Urho3D::VariantMap& eventData);

	float getHeightAt(const float x, const float y) {
		return _nz.get_noize(x, y);
	}

    void resetNumTilesLoadedCounters () {
        _numInnerTilesLoadedCounter = 0;
        _numOuterTilesLoadedCounter = 0;
    }

    std::pair<size_t, size_t> getNumTilesLoadedCounters () {
        return { _numInnerTilesLoadedCounter,
                 _numOuterTilesLoadedCounter };
    }

private:
    void fillPrecalcPositions ();

    void updateOuterTiles();

    bool isOutsideOuterDistance (const Urho3D::IntVector2 &pos_2d,
                                 const Urho3D::IntVector2 &pos_i);
    bool workIsInProgress (const Urho3D::IntVector2 &pTilePos,
                           const unsigned pResolution);

    bool isUpdateNeeded ();

    void loadTile(ModelLoadWorkerData *dt);
    void loadOuterTile(const ModelLoadWorkerData *dt);
    void eraseCurrentWork (ModelLoadWorkerData *dt);


    Settings *_cfg = nullptr;
    std::unordered_map<Urho3D::IntVector2,
                       MapTileData, hash_int_vector2> _mapTiles;
    std::unordered_map<Urho3D::IntVector2,
                       Urho3D::Node*,
                       hash_int_vector2> _outerMapTiles;
    Urho3D::Node *_cameraNode = nullptr;

    std::unordered_multimap<Urho3D::IntVector2,
                            ModelLoadWorkerData,
                            hash_int_vector2> _currentWorks;

    std::unordered_map<Urho3D::IntVector2,
                       Urho3D::UIElement*,
                       hash_int_vector2> _dbgMapElements;
    std::unordered_map<Urho3D::IntVector2,
                       Urho3D::UIElement*,
                       hash_int_vector2> _dbgOuterMapElements;


    Urho3D::IntVector2 _currentCamPos; // current position of camera at the map
                                       // in terms of our map tiles system

    unsigned _mapTileSize = 0; // cached from settings, so that we don't gather it from
                      // settings every time;

    unsigned _outerMapTileSize = 0;
    unsigned _outerMapTileResolution = 0 ;

    Urho3D::UIElement* _dMapRoot = nullptr;

    size_t _numInnerTilesLoadedCounter = 0;
    size_t _numOuterTilesLoadedCounter = 0;

    float start_load_time = 0;
    float first_load_time = 0;

    bool _showDbgMap = false;
    Noizes _nz;

    bool _waitingToFinishTasksBeforeRestart = false;

    struct LandscapeGenCache {
    public:
        std::unique_ptr<UrhoworldsMemory<UrhoworldsIndexElement>>
        getFreeIndexVector (const unsigned res_x,
                            const unsigned res_y) {
            const size_t size = size_t(res_x)
                * size_t(res_y) * 2;
            if (_index.size()) {
                _index.back()->resize(size);
                auto ret = std::move(_index.back());
                _index.pop_back();
                return ret;
            }

            std::unique_ptr<UrhoworldsMemory<UrhoworldsIndexElement>>
                ret (new UrhoworldsMemory<UrhoworldsIndexElement>(size));
            ret->resize(size);
            return ret;
        }
        std::unique_ptr<UrhoworldsMemory<UrhoworldsLandscapeVertexElement>>
        getFreeVertexVector (const unsigned res_x,
                             const unsigned res_y) {
            const size_t size = (size_t(res_x) + 1)
                * (size_t(res_y) + 1);
            if (_vertex.size()) {
                _vertex.back()->resize(size);
                auto ret = std::move(_vertex.back());
                _vertex.pop_back();
                return ret;
            }

            std::unique_ptr<UrhoworldsMemory<UrhoworldsLandscapeVertexElement>>
                ret(new UrhoworldsMemory<UrhoworldsLandscapeVertexElement>(size));

            ret->resize(size);

            return ret;
        }

        void returnIndexVectorToCache (std::unique_ptr<UrhoworldsMemory<UrhoworldsIndexElement>> &vec) {
            _index.push_back(std::move(vec));
        }

        void returnVertexVectorToCache (std::unique_ptr<UrhoworldsMemory<UrhoworldsLandscapeVertexElement>> &vec) {
            _vertex.push_back(std::move(vec));
        }

    private:
        unsigned _resX = 0;
        unsigned _resY = 0;
        std::vector<std::unique_ptr<UrhoworldsMemory<UrhoworldsIndexElement>>> _index;
        std::vector<std::unique_ptr<UrhoworldsMemory<UrhoworldsLandscapeVertexElement>>> _vertex;
    };
    LandscapeGenCache _cacheLod0;
    LandscapeGenCache _cacheLod1;

    LandscapeManager _landscapeManager;
    LandscapeManager _outerLandscapeManager;

    Urho3D::SharedPtr<Urho3D::Impostor> _tree_imp;
};
