// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com
#include <Impostor.hpp>

#include <Urho3D/Core/Context.h>
#include <Urho3D/Scene/Scene.h>
#include <Urho3D/Scene/SceneEvents.h>
#include <Urho3D/Graphics/Octree.h>
#include <Urho3D/Graphics/Renderer.h>
#include <Urho3D/Graphics/StaticModel.h>
#include <Urho3D/Resource/ResourceCache.h>
#include <Urho3D/Graphics/Material.h>
#include <Urho3D/Graphics/Viewport.h>
#include <Urho3D/Graphics/Camera.h>
#include <Urho3D/Graphics/Texture2D.h>
#include <Urho3D/Graphics/Graphics.h>
#include <Urho3D/Graphics/Technique.h>
#include <Urho3D/Graphics/Octree.h>
#include <Urho3D/Graphics/RenderPath.h>
#include <Urho3D/Graphics/Model.h>
#include <Urho3D/Graphics/GraphicsEvents.h>
#include <Urho3D/Graphics/View.h>
#include <Urho3D/Graphics/OcclusionBuffer.h>
#include <Urho3D/UI/UI.h> // for XMLFile hmm?
#include <Urho3D/IO/Log.h>
#include <Urho3D/IO/FileSystem.h>
#include <Urho3D/Core/Profiler.h>

#include "LandscapeGenerator.hpp"

#include <algorithm>
#include <unordered_map>

#include "Utils/MathUtils.hpp"

namespace Urho3D {
constexpr int impostor_resolution = 5;
constexpr size_t impostor_segment_size = 1024;
    constexpr size_t impostor_texture_resolution = impostor_segment_size * impostor_resolution;

struct ImpostorCuratorData {
    // TODO consider impostor be a key in std::unordered_map or something
    Impostor* impostor;
    Node* node;
    int distance;
};

struct ImpostorCuratorSceneData {
    std::vector<ImpostorCuratorData> impostors;
    Node* cameraNode = nullptr;
};

class ImpostorsCurator : public Object {
    URHO3D_OBJECT(ImpostorsCurator, Object);
public:
    ImpostorsCurator(Context *context)
    : Object(context) {
    }


    void ImpostorCreated (Texture2D* tx, Impostor *imp, Node* node, int distance) {
        URHO3D_PROFILE(ImpostorCuratorImpostorCreated);
        impostors_[tx].impostors.push_back({imp, node, distance});
        dirty = true;
    }

    void ImpostorInstanceDestroyed(Texture2D* tx, Impostor *imp) {
        URHO3D_PROFILE(ImpostorCuratorInstanceDestroyed);
        auto it = std::find_if(
            impostors_[tx].impostors.begin(),
            impostors_[tx].impostors.end(),
            [&imp](const ImpostorCuratorData& x) {
                return x.impostor == imp;
            });

        if(it == impostors_[tx].impostors.end()) {
            throw std::runtime_error("impostor destroyed: not present");
        }

        std::swap(*it, *(impostors_[tx].impostors.end()-1));
        impostors_[tx].impostors.pop_back();
    }

    // TODO make protected and make Impostor friend of this
    void ImpostorReady(Texture2D* tx) {
        URHO3D_PROFILE(ImpostorCuratorImpostorReady);
        impostors_[tx].cameraNode =
            impostors_[tx].impostors[0].node->GetScene()->GetChild("Camera",
                                                                   true);
        if(!listeningUpdates_) {
            SubscribeToEvent(E_ENDVIEWRENDER,
                             URHO3D_HANDLER(ImpostorsCurator, HandleUpdate));
            listeningUpdates_ = true;
        }

        dirty = true;
    }

    void HandleUpdate (StringHash,
                       VariantMap& evtData) {
        for (auto els : impostors_) {
            if (!els.second.cameraNode) {
                continue;
            }
            auto scn = els.second.cameraNode->GetScene();
            if (evtData[EndViewRender::P_SCENE] != scn) {
                // we assume that our ImpostorCurator might be ran for multiple
                // scenes
                continue;
            }
            const auto cam_pos = els.second.cameraNode->GetWorldPosition();
            if (cam_pos.Equals(prev_cam_pos) && !dirty) {
                continue;
            }
            prev_cam_pos = cam_pos;
            for (auto el : els.second.impostors) {
                URHO3D_PROFILE(ImpostorCuratorUpdate);

                const auto imp_pos = el.node->GetWorldPosition();

                const auto camera_direction = cam_pos - imp_pos;
                float len = camera_direction.Length();
                el.impostor->SwitchActive(len > abs(el.distance - 1));
            }
        }
        dirty = false;
    }

private:
    std::unordered_map<Texture2D*, ImpostorCuratorSceneData> impostors_;
    bool listeningUpdates_ = false;
    Vector3 prev_cam_pos = Vector3(1000000000,
                                   1000000000,
                                   1000000000);
    bool dirty = true;
};

Impostor::Impostor(Context *context)
    : Component(context),
      buffer(new Image(context))
{
    // TODO: require user do it themselves?
    if (!GetSubsystem<ImpostorsCurator>()) {
        context->RegisterSubsystem(new ImpostorsCurator(context));
    }

    material_ = GetSubsystem<ResourceCache>()
        ->GetResource<Material>("Misc/OctahedralImpostor.xml")->Clone();
    material_->SetShaderParameter("ImpostorResolution", impostor_resolution);

    screenTexture_ = new Texture2D(context_);
    screenTextureBump_ = new Texture2D(context_);
}

// TODO3: delete all the relevant stuff (scene, octree, etc..)
Impostor::~Impostor()
{
}

void Impostor::RegisterObject(Context* context)
{
    context->RegisterFactory<Impostor>();
}

Impostor::Impostor (const Impostor &pOther)
    : Component (pOther.context_) {
    amCopied_ = true;
    actAsBillboard_ = pOther.actAsBillboard_;
    distance_ = pOther.distance_;
    needRegen_ = false;
    bounds_ = pOther.bounds_;
    screenTexture_ = pOther.screenTexture_;
    screenTextureBump_ = pOther.screenTextureBump_;
    material_ = pOther.material_;
    material_->SetShaderParameter("ImpostorResolution", impostor_resolution);
    plane_ = pOther.plane_;
}

Impostor* Impostor::Clone() {
    auto ret = new Impostor(*this);
    return ret;
}


void Impostor::OnNodeSet(Node* node)
{
    if (node == nullptr) {
        CleanUp();
    } else {
        if (!amCopied_) {
        }
        sceneCamera_ = node_->GetScene()->GetChild("Camera", true);

        if (amCopied_) {
            material_->SetTexture(TU_DIFFUSE, screenTexture_);
            material_->SetTexture(TU_NORMAL, screenTextureBump_);
            // TODO: not needed / no effect?
            UnsubscribeFromEvent(E_ENDVIEWRENDER);
        }
        GetSubsystem<ImpostorsCurator>()->ImpostorCreated(screenTexture_,
                                                          this,
                                                          node_,
                                                          distance_);
    }
}

void Impostor::OnMarkedDirty(Node* node) {
    if (node) {
        if (amCopied_) {
            SetupScreen();
        }
        if (needRegen_) {
            CreateImpostor();
            needRegen_ = false;
        }
    }
}

void Impostor::CleanUp() {
    GetSubsystem<ImpostorsCurator>()
        ->ImpostorInstanceDestroyed(screenTexture_, this);
}


// TODO handle cases where children has their own transform
void Impostor::ReassembleModel()
{
    PODVector<StaticModel*> static_models;
    PODVector<StaticModel*> static_models_nonrecursive;
    node_->GetComponents<StaticModel> (static_models, true /*recursive*/);
    node_->GetComponents<StaticModel> (static_models_nonrecursive, false);

    if (static_models_nonrecursive.Size() > static_models.Size()) {
        URHO3D_LOGWARNING("Impostor: child nodes assumed to have no"
                          " transform");
    }

    impObjectNode_ = impScene_->CreateChild();

    bounds_ = BoundingBox();
    for (StaticModel* s_m : static_models) {
        StaticModel* mdl = impObjectNode_->CreateComponent<StaticModel>();
        mdl->SetModel(s_m->GetModel());
        auto mat = s_m->GetMaterial();
        auto my_bump_pass = mat->GetTechnique(0)->CreatePass("my_bump_pass");
        my_bump_pass->SetPixelShader("Imp");
        my_bump_pass->SetVertexShader("Imp");

        mdl->SetMaterial(s_m->GetMaterial());

        bounds_.Merge(s_m->GetModel()->GetBoundingBox());
    }
}


float getClosestCoord (const float coord,
                       const int impostorResolution) {
    float res = (float) impostorResolution;
    float rounded = std::round(coord * res);
    return (float)rounded / res;
}

Vector2 getClosestCoord (const Vector2 &coord,
                         const int impostorResolution) {
    return Vector2(getClosestCoord(coord.x_, impostorResolution),
                   getClosestCoord(coord.y_, impostorResolution));
}


Vector2 getUvCoordByNum (const int num, const int impostorResolution) {
    const float base_offset = 1.f / ((float) impostorResolution);
    const float horizontal_offset =
        ((float)(num % impostorResolution)) * base_offset;
    const float vertical_offset =
        ((float)(num / impostorResolution)) * base_offset;

    Vector2 ret = Vector2(vertical_offset,
                          horizontal_offset);
    return ret;
}

Vector2 getCamCoordByNum (const int num, const int impostorResolution) {
    const float base_offset = 2.f / ((float)impostorResolution - 1);
    const float horizontal_offset =
        ((float)(num % impostorResolution)) * base_offset;
    const float vertical_offset =
        ((float)(num / impostorResolution)) * base_offset;

    Vector2 ret = Vector2(-1.f + vertical_offset,
                          -1.f + horizontal_offset);
    return ret;
}

float sign (const float val) {
    return (val > 0) ? 1.f : -1.f;
}

// input coordinates are ranged from -1..1, not 0..1
Vector3 UVtoPyramid(Vector2 uv) {
    float leftright_negation = 1 - std::abs(uv.x_);
    float frontback_negation = 1 - std::abs(uv.y_);

    float up = std::min(leftright_negation, frontback_negation);
    Vector3 position (uv.x_, up, uv.y_);

    return position.Normalized();
}

float sqr(float val) {
	return val * val;
}

float calc_simple_square_stretching(float x, float y) {
    // https://arxiv.org/ftp/arxiv/papers/1509/1509.06344.pdf
    // page 3, Disc to square mapping:
    if (sqr(x) >= sqr(y)) {
        return sign(x) * (std::sqrt(sqr(x) + sqr(y)));
    }
    return  sign(y) * x / y * (std::sqrt(sqr(x) + sqr(y)));
}

Vector2 calc_simple_square_stretching (Vector2 in) {
    return { calc_simple_square_stretching(in.x_, in.y_),
             calc_simple_square_stretching(in.y_, in.x_) };
}

// returns -1..1 coordinates
Vector2 PyramidUV(const Vector3 &direction) {
    Vector3 dir = direction.Normalized();
    Vector2 uv;

    uv.x_ = calc_simple_square_stretching(dir.x_, dir.z_);
    uv.y_ = calc_simple_square_stretching(dir.z_, dir.x_);

    return uv / (1 + dir.y_);
}

Vector2 GetCurrentUV (const Vector3 camera_direction,
                      const int impostor_resolution) {
    const float imp_res = float(impostor_resolution);

    auto puv = PyramidUV(camera_direction);

    Vector2 pyr =
        (puv + Vector2(1.f, 1.f))
        * 0.5f;

    return pyr;
}

float get_next_coord (const float base,
                      const float offset,
                      const float imp_res) {
    return { base + 1.f / (imp_res) * (offset >= 0 ? 1.f : -1.f) };
}
struct ImpostorUvData {
    Vector2 offset1, offset2, offset3, offset4;
    float factor1, factor2, factor3, factor4;
};

ImpostorUvData GetCurrentUVs (const Vector2 curr,
                              const int impostor_resolution) {
    const auto base = getClosestCoord(curr, impostor_resolution - 1);
    const auto offset = curr - base;

    Vector2 curr_amts = { 1.f - std::abs(offset.x_ * (impostor_resolution - 1)),
                          1.f - std::abs(offset.y_ * (impostor_resolution - 1)) };

    const Vector2 next_x =
        { get_next_coord(base.x_, offset.x_, impostor_resolution - 1),
          base.y_ };
    const Vector2 next_y =
        { base.x_,
          get_next_coord(base.y_, offset.y_, impostor_resolution - 1) };

    const Vector2 next_xy =
        { next_x.x_,
          next_y.y_, };

    float conv = (impostor_resolution - 1.f) / impostor_resolution;

    return { base * conv ,
             next_x * conv,
             next_y * conv,
             next_xy * conv,

             curr_amts.x_ * curr_amts.y_,
             curr_amts.y_ * (1.f - curr_amts.x_),
             curr_amts.x_ * (1.f - curr_amts.y_),
             (1.f - curr_amts.x_) * (1 - curr_amts.y_),
    };
}

void Impostor::SetCameraPosition() {
    Camera *cam = impCameraNode_->GetComponent<Camera>();

    const Vector3 obj_size = bounds_.Size() * impostorMarginMultiplier;

    float max_dimention = std::max(obj_size.x_,
                                   std::max(obj_size.y_,
                                            obj_size.z_));

    Vector2 cam_coord =
        getCamCoordByNum(impostorFrameNumber, impostor_resolution);

    Vector3 cam_pos_shift = UVtoPyramid(cam_coord);

    cam_pos_shift *= max_dimention;
    Vector3 cam_pos = (bounds_.Center() * boundsMask_) + cam_pos_shift;

    cam->SetOrthographic(true);
    cam->SetOrthoSize(max_dimention);
    cam->SetAspectRatio(1.f);
    cam->SetNearClip(cam_pos_shift.Length() - max_dimention / 2.f);
    cam->SetFarClip(cam_pos_shift.Length() + max_dimention / 2.f);
    impCameraNode_->SetPosition(cam_pos);
    auto dbg_offset = Vector3(0.f, 1.f, 0.f) * 0.f; // TODO remove
    impCameraNode_->LookAt(bounds_.Center() + dbg_offset);
}

SharedPtr<Model> Impostor::CreatePlane (const float uv_incr,
                                        const Vector2 &uv_base,
                                        const float scale) {
    UrhoworldsPlaneConstructionData<UrhoworldsTexturedVertexElement> dt;

    dt.vertex_data->push_back({ Vector3{ -1, -1, 0 } * scale,
                                Vector3{ 0, 0, -1},
                                uv_base + Vector2{ 0, uv_incr } });

    dt.vertex_data->push_back({ Vector3{ -1, 1, 0 } * scale,
                                Vector3{ 0, 0, -1},
                                uv_base });
    dt.vertex_data->push_back({ Vector3{ 1, -1, 0 } * scale,
                                Vector3{ 0, 0, -1},
                                uv_base + Vector2(uv_incr, uv_incr) });
    dt.vertex_data->push_back({ Vector3{ 1, 1, 0 } * scale,
                                Vector3{ 0, 0, -1},
                                uv_base + Vector2{ uv_incr, 0 } });

    const Vector3 bbmin (-1, -1, 0);
    const Vector3 bbmax (1, 1, 0);

    dt.index_data->push_back({0, 1, 2});
    dt.index_data->push_back({2, 1, 3});

    return create_1lod_plane(context_, dt, bbmin, bbmax);
}

void Impostor::StartImpostorRender()
{
    impostorFrameNumber = 0;

    SetCameraPosition();

    SubscribeToEvent (E_ENDVIEWRENDER,
                      URHO3D_HANDLER(Impostor, HandleRenderDone));

    impRenderTexture_->GetRenderSurface()->QueueUpdate();

    ++impostorFrameNumber;
}

void Impostor::GatherImpostorPart(Texture2D *tex,
                                  Image* img,
                                  const Vector2 &uv_coord) {
    // TODO we should do it when start rendering...
    buffer->SetSize(tex->GetWidth(),
                    tex->GetHeight(),
                    tex->GetComponents());

    tex->GetData(0, buffer->GetData());

    img->SetSubimage(buffer,
                     IntRect(int(uv_coord.x_),
                             int(uv_coord.y_),
                             int(uv_coord.x_) + tex->GetWidth(),
                             int(uv_coord.y_) + tex->GetHeight()));
}

void Impostor::ProcessImpostorPartRendered() {


    if (impostorFrameNumber == 1) {
            if (!impResultImage_) {
                impResultImage_ = new Image(context_);
                impResultImage_->SetSize(impostor_texture_resolution,
                                         impostor_texture_resolution,
                                         impRenderTexture_->GetComponents());
            }

            if (!impResultDepthImage_) {
                impResultDepthImage_ = new Image(context_);
                impResultDepthImage_->SetSize(impostor_texture_resolution,
                                              impostor_texture_resolution,
                                              impRenderTextureDepth_->GetComponents());
            }
        }

        if (impostorFrameNumber <= impostor_resolution * impostor_resolution) {
            Vector2 uv_coord =
                getUvCoordByNum(impostorFrameNumber - 1, impostor_resolution)
                * float(impostor_texture_resolution);

            GatherImpostorPart(impRenderTexture_,
                               impResultImage_,
                               uv_coord);
            GatherImpostorPart(impRenderTextureDepth_,
                               impResultDepthImage_,
                               uv_coord);
            //GatherImpostorDepthPart(uv_coord);
            SetCameraPosition();
            impRenderTexture_->GetRenderSurface()->QueueUpdate();
            ++impostorFrameNumber;
        }

        if (impostorFrameNumber == impostor_resolution * impostor_resolution + 1)  {
            //ExportImage(impResultImage_, "impostor");
            //ExportImage(impResultDepthImage_, "impostor_depth");
            screenTexture_->SetSize(impostor_texture_resolution,
                                    impostor_texture_resolution,
                                    Graphics::GetRGBAFormat(),
                                    Urho3D::TEXTURE_STATIC);
            screenTexture_->SetData(impResultImage_);
            material_->SetTexture(TU_DIFFUSE, screenTexture_);


            screenTextureBump_->SetSize(impostor_texture_resolution,
                                        impostor_texture_resolution,
                                        Graphics::GetRGBAFormat(),
                                        Urho3D::TEXTURE_STATIC);
            screenTextureBump_->SetData(impResultDepthImage_);
            material_->SetTexture(TU_NORMAL, screenTextureBump_);

            UnsubscribeFromEvent (E_ENDVIEWRENDER);
            GetSubsystem<ImpostorsCurator>()->ImpostorReady(screenTexture_);
        }
}


void Impostor::UpdateImpostorScreen(Vector3 camera_direction) {
    URHO3D_PROFILE(ImpostorUpdateScreen);
    const float imp_res = (float)impostor_resolution;

    
    if (camera_direction.y_ < 0.f) {
        camera_direction.y_ = 0.f;
    }

    camera_direction = node_->GetRotation().Inverse() * camera_direction;
    // material_->SetShaderParameter("ImpostorCameraPosition",
    //                               sceneCamera_->GetWorldPosition());

    const auto pyr = GetCurrentUV(camera_direction,
                                  impostor_resolution);
    const auto uv = GetCurrentUVs(pyr * (impostor_resolution
                                         / (impostor_resolution - 1)),
                                  impostor_resolution);

    // TODO2: 
    // material_->SetShaderParameter("ImpostorResolution",
    //                               (float)impostor_resolution);


    //material_->SetShaderParameter("ImpostorParentRotMatrix",
    //                                  node_->GetTransform().ToMatrix3());

    URHO3D_PROFILE(ImpostorUpdateScreen4);
    if (actAsBillboard_) {
        // screenRootNode_->LookAt(sceneCamera_->GetPosition());
    } else {

        // const auto pp = UVtoPyramid(uv.offset1  * (imp_res / (imp_res - 1)) * 2 - Vector2(1,1));
        // screenRootNode_->LookAt(node_->GetTransform().ToMatrix3() * pp  * 10
        //                         + screenRootNode_->GetWorldPosition(),
        //                         Vector3::UP,
        //                         TS_WORLD);

                                }
}

void Impostor::HandleRenderDone (StringHash,
                                 VariantMap& eventData) {
    URHO3D_PROFILE(ImpostorUpdate);
    if (!amCopied_
        && impRenderTexture_
        && eventData[EndViewRender::P_SURFACE].GetPtr()
           == impRenderTexture_->GetRenderSurface()) {
        ProcessImpostorPartRendered();
    }
}

void Impostor::SwitchActive(const bool active) {
    screenNode_->SetDeepEnabled(active);
    PODVector<StaticModel*> dst;
    node_->GetComponents<StaticModel>(dst);
    for (auto el : dst) {
        el->SetEnabled(!active);
    }
}

void Impostor::CreateImpostor()
{
    if (!impScene_) {
        impScene_ = new Scene(context_);
        impScene_->CreateComponent<Octree>();

        // add light (should probably set ambient color to 1 1 1)
        Node* lightNode = impScene_->CreateChild(); 
        lightNode->SetDirection(Vector3(1.f, -0.3f, 0.0f));

        Light* light = lightNode->CreateComponent<Light>();
        light->SetLightType(LIGHT_DIRECTIONAL);
        light->SetColor({ 0.01f, 0.01f, 0.01f });

        impCameraNode_ = impScene_->CreateChild();
        impCameraNode_->CreateComponent<Camera>();

        // create rgba texture
        const int segment_res
            = impostor_texture_resolution / impostor_resolution;
        impRenderTexture_ = new Texture2D(context_);
        impRenderTexture_->SetSize(segment_res,
                                   segment_res,
                                   Graphics::GetRGBAFormat(),
                                   Urho3D::TEXTURE_RENDERTARGET,
                                   1);

        impRenderTexture_->SetFilterMode(FILTER_NEAREST);

        auto cache = GetSubsystem<ResourceCache>();
        if (!cache->GetResource<Texture2D>("my_bump")) {
            impRenderTextureDepth_ = new Texture2D(context_);
            impRenderTextureDepth_->SetSize(segment_res,
                                            segment_res,
                                            Graphics::GetRGBAFormat(),
                                            Urho3D::TEXTURE_RENDERTARGET,
                                            1);
            impRenderTextureDepth_->SetName("my_bump");
            cache->AddManualResource(impRenderTextureDepth_);
        } else {
            impRenderTextureDepth_ = cache->GetResource<Texture2D>("my_bump");;
        }

        impRenderPath_ = new RenderPath();
        impRenderPath_->Load(
            GetSubsystem<ResourceCache>()
            ->GetResource<XMLFile>("RenderPaths/ForwardWithBumpBake.xml"));


        RenderSurface* renderSurface = impRenderTexture_->GetRenderSurface();

        impViewport_ = new Viewport(context_,
                                    impScene_,
                                    impCameraNode_->GetComponent<Camera>());
        impViewport_->SetRenderPath(impRenderPath_);
        renderSurface->SetUpdateMode(Urho3D::SURFACE_MANUALUPDATE);
        //renderSurface->SetLinkedRenderTarget(
        //impRenderTextureDepth_->GetRenderSurface());
        //renderSurfaceDepth->SetViewport(1, impViewport_);
        renderSurface->SetViewport(0, impViewport_);
    } else {
        //impScene_->RemoveChild(impObjectNode_);
    }


    ReassembleModel();
    Zone* a_zone = new Zone(context_);
    impObjectNode_->GetComponent<StaticModel>()->SetZone(a_zone);
    impObjectNode_->GetComponent<StaticModel>()->GetZone()->SetAmbientColor({1.f, 1.f, 1.f});

    const Vector3 obj_size = bounds_.Size() * impostorMarginMultiplier;
    float max_dimention = std::max(obj_size.x_, std::max(obj_size.y_, obj_size.z_));

    ResourceCache* cache = GetSubsystem<ResourceCache>();

    StartImpostorRender();

    SetupScreen();
}

void Impostor::SetupScreen() {
    screenNode_ = node_->GetScene()->CreateChild();
    screenNode_->SetRotation(node_->GetWorldRotation());
    // TODO make sure  we update it if need

    screenNode_->SetPosition(Vector3(0.f, bounds_.Center().y_, 0.f) + node_->GetWorldPosition());
    screenNode_->Translate(positionOffset_,
                           positionOffsetUseParentSpace_ ? TS_PARENT : TS_WORLD);

    const Vector3 obj_size = bounds_.Size() * impostorMarginMultiplier;

    float max_dimention = std::max(obj_size.x_,
                                   std::max(obj_size.y_,
                                            obj_size.z_));

    // I am not good with 3d-math so I resorted to using a proxy parent node to
    // make my life easier...
    screenNode_->SetScale(Vector3(max_dimention, max_dimention, 1));
    StaticModel* screenObject = screenNode_->CreateComponent<StaticModel>();
    if (!plane_) {
        plane_ = CreatePlane(1.f, Vector2(0, 0), .5f);
    }
    screenObject->SetModel(plane_);

    // material_->SetTexture(TU_DIFFUSE,
    //                       cache->GetResource<Texture2D>("Utils/Checker.png"));

    screenObject->SetMaterial(material_);
}

void Impostor::ExportImage(Image* img, const String &name) {
    String pathName(GetSubsystem<FileSystem>()->GetProgramDir());
    String filePath(pathName +
                    String(size_t(this)) +
                    "_" +
                    String(impostorFrameNumber) +
                    name +
                    ".png");

    img->SavePNG(filePath);
}

} // namespace Urho3D
