#pragma once


#include <string>
#include <cmath>
#include <unordered_set>
#include <iterator>
#undef max
#include <limits>


#include <Urho3D/Math/Vector2.h>
#include <Urho3D/Math/Vector3.h>

namespace {
template<typename T>
T sqr (T pVal) {
    return pVal * pVal;
}


template<typename I, typename L, typename H>
auto _clamp (const I& pVal, const L &lo, const H &hi) ->decltype(pVal+lo+hi) {
    if (pVal < lo) {
        return lo;
    } else if (pVal > hi) {
        return hi;
    }
    return pVal;
}
}

struct hash_int_vector2 {
    size_t operator()(const Urho3D::IntVector2 &pVec) const noexcept {
        auto h1 = std::hash<int>()(pVec.x_);
        auto h2 = std::hash<int>()(pVec.y_);
        return ((h1 << (sizeof(size_t)/2)) | (h1 >> (sizeof(size_t)/2))) ^ h2;
    }
};


using SetIntVector2 = std::unordered_set<Urho3D::IntVector2, hash_int_vector2>;


class LandscapeManager {
public:
    LandscapeManager (const size_t pNumTilesToRender = 0,
                      const Urho3D::Vector3 &pCameraPosition = Urho3D::Vector3(),
                      const Urho3D::Vector3 &pCameraDirection = Urho3D::Vector3()) {
        _setCameraTransform(pCameraPosition, pCameraDirection);
        _setNumTilesToRender(pNumTilesToRender),
        reinit();
    }

    void setNumExcludedTilesToRender (const size_t pNum) {
        _numExcludedTilesToRender = pNum;
    }

    void reinit () {
        _positions.clear();
        _loadedPositions.clear();
        recompute();
    }

    void setCurrentCameraTransform (const Urho3D::Vector3 &pPosition,
                                    const Urho3D::Vector3 &pDirection) {
        _setCameraTransform(pPosition, pDirection);
        recompute();
    }

    /// e.g. 3 means => 1 tile exactly below the camera + 2 tiles on every
    /// direction totaling to 5by5 square
    void setNumTilesToRender (const size_t pNum) {
        _setNumTilesToRender(pNum);
        recompute();
    }

    void recompute () { // TODO make private
        const int edges = int(_numTilesToRender) - 1;

        _positions.clear();

        for (int i = -edges; i <= edges; ++i) {
            for (int j = -edges; j <= edges; ++j) {
                auto new_pos = Urho3D::IntVector2(i, j) + _camPosition;
                if (!getTileIsLoaded(new_pos)
                    && (std::abs(i) >= _numExcludedTilesToRender
                        || std::abs(j) >= _numExcludedTilesToRender))
                    _positions.insert(new_pos);
            }
        }

        for (auto it = _loadedPositions.begin();
             it != _loadedPositions.end();) {
            if (getTileIsNeeded(*it)) {
                ++it;
            } else {
                _unneededPositions.insert(*it);
                it = _loadedPositions.erase(it);
            }
        }
    }

    size_t getNumTilesToLoad () const {
        return _positions.size();
    }

    Urho3D::IntVector2 getNthTilePositionToLoad (const size_t n) const {
        auto it = _positions.begin();
        std::advance(it, n);
        return *it;
    }

    const SetIntVector2& getAllElementsToLoad () const {
        return _positions;
    }

    const SetIntVector2& getAllLoadedElements () const {
        return _loadedPositions;
    }

    const SetIntVector2& getAllElementsToUnload () const {
        return _unneededPositions;
    }


    void setTileIsLoaded (const Urho3D::IntVector2 &pTilePos) {
        auto found = _positions.find(pTilePos);
        if (found != std::end(_positions)) {
            _loadedPositions.insert(pTilePos);
            _positions.erase(found);
        } else {
            // already loaded, the caller should keep track, or manually check
            // which tiles are needed
            throw;
        }
    }

    bool getTileIsNeeded (const Urho3D::IntVector2 &pTilePos) const {
        return std::abs(pTilePos.x_ - _camPosition.x_) < _numTilesToRender
            && std::abs(pTilePos.y_ - _camPosition.y_) < _numTilesToRender
            && (std::abs(pTilePos.x_ - _camPosition.x_) >= _numExcludedTilesToRender
                || std::abs(pTilePos.y_ - _camPosition.y_) >= _numExcludedTilesToRender);
    }

    bool getTileIsLoaded (const Urho3D::IntVector2 &pTilePos) const {
        return _loadedPositions.find(pTilePos) != std::end(_loadedPositions);
    }

    bool getTileIsOughtToBeLoaded (const Urho3D::IntVector2 &pTilePos) const {
        return _positions.find(pTilePos) != std::end(_positions);
    }

    void clearTilesToUnload () {
        _unneededPositions.clear();
    }

private:
    void _setCameraTransform (const Urho3D::Vector3 &pPosition,
                              const Urho3D::Vector3 &pDirection) {
        _camAbsolutePosition = pPosition;

        _camPosition = Urho3D::IntVector2(static_cast<int>(round(pPosition.x_)),
                                          static_cast<int>(round(pPosition.z_)));
        _camDirection = pDirection;
    }

    void _setNumTilesToRender (const size_t pNum) {
        _numTilesToRender = pNum;

        if (_numTilesToRender > size_t(std::numeric_limits<int>::max())) {
            throw;
        }
    }

    Urho3D::IntVector2 _camPosition;
    Urho3D::Vector3 _camAbsolutePosition;
    Urho3D::Vector3 _camDirection;
    SetIntVector2 _positions;
    SetIntVector2 _loadedPositions;
    SetIntVector2 _unneededPositions;
    size_t _numTilesToRender;
    size_t _numExcludedTilesToRender = 0;
};
