#pragma once


#include <Urho3D/Core/Object.h>
#include <Urho3D/Engine/Console.h>
#include <Urho3D/Scene/Node.h>


class Settings;


class UrhoConsole : public Urho3D::Object {
    URHO3D_OBJECT(UrhoConsole, Urho3D::Object);
public:
    explicit UrhoConsole (Urho3D::Context *pContext);

    void setSettingsPtr (Settings *pSettings);
    void setCameraNodePtr (Urho3D::Node *pCameraNode);

    void start ();
    void handleConsoleCommand (Urho3D::StringHash eventType,
                               Urho3D::VariantMap& eventData);
    void handleConsoleInput (const Urho3D::String& pInput);
    void printToConsole (const Urho3D::String& pOutput);

private:
    Urho3D::Node *_cameraNode = nullptr;
    Urho3D::Console *_console = nullptr;
    Settings *_cfg = nullptr;
};
