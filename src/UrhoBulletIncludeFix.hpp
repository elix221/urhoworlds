#pragma once

#ifdef new
#define _urhobulletworkaround_oldnew_ new
#define _urhobulletworkaround_fix_is_applied_
#undef new
#endif

#include <Urho3D/Physics/CollisionShape.h>
#include <Urho3D/Physics/RigidBody.h>
#include <Urho3D/Physics/PhysicsWorld.h>
#include <Urho3D/ThirdParty/Bullet/BulletCollision/CollisionShapes/btTriangleMesh.h>
#include <Urho3D/ThirdParty/Bullet/BulletCollision/CollisionShapes/btBvhTriangleMeshShape.h>
#include <Urho3D/ThirdParty/Bullet/BulletDynamics/Dynamics/btRigidBody.h>


#ifdef _urhobulletworkaround_fix_is_applied_
#define new _urhobulletworkaround_oldnew_
#endif

