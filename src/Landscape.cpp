// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com
#include "Landscape.hpp"

#include <random>

#include <Urho3D/Core/CoreEvents.h>
#include <Urho3D/Core/Profiler.h>
#include <Urho3D/Core/WorkQueue.h>
#include <Urho3D/Graphics/Material.h>
#include <Urho3D/Graphics/StaticModel.h>
#include <Urho3D/Graphics/StaticModelGroup.h>
#include <Urho3D/IO/Log.h>
#include <Urho3D/Resource/ResourceCache.h>
#include <Urho3D/UI/BorderImage.h>
#include <Urho3D/UI/Font.h>
#include <Urho3D/UI/Text.h>
#include <Urho3D/UI/Text3D.h>
#include <Urho3D/UI/UI.h>

#include "TreeGenerator.hpp"
#include "Impostor.hpp"

using namespace Urho3D;

int round_up (int numToRound, int multiple) {
    int isPositive = (int)(numToRound >= 0);
    return ((numToRound + isPositive * (multiple - 1)) / multiple) * multiple;
}

btIndexedMesh landscape_create_phys_mesh (const ModelLoadWorkerData *dt) {
    btIndexedMesh mesh;

    mesh.m_numTriangles        = int(dt->physics_data->index_data->size());
    mesh.m_triangleIndexBase   =
        (const unsigned char *) dt->physics_data->index_data->data();
    mesh.m_triangleIndexStride = 3 * sizeof(unsigned short);
    mesh.m_numVertices         = int(dt->physics_data->vertex_data.size());
    mesh.m_vertexBase          =
        (const unsigned char *)dt->physics_data->vertex_data.data();
    mesh.m_vertexStride        = sizeof(Urho3D::Vector3);

    return mesh;
}

void landscape_load_physics (ModelLoadWorkerData* dt) {
    dt->physics_data = std::make_unique<UrhoworldsPhysicsData>();

    dt->physics_data->index_data = dt->lod0.index_data.get();

    dt->physics_data->vertex_data.reserve(dt->lod0.vertex_data->size());
    for (size_t i = 0; i < dt->lod0.vertex_data->size(); ++i) {
        auto el = (*dt->lod0.vertex_data.get())[i];
        dt->physics_data->vertex_data.push_back(el.coord);
    }

    dt->physics_data->bt_mesh.addIndexedMesh(landscape_create_phys_mesh(dt),
                                             PHY_SHORT);
    dt->physics_data->bt_shape =
        std::make_unique<btBvhTriangleMeshShape>(&dt->physics_data->bt_mesh,
                                                 true);
}

void landscape_load_trees_part (ModelLoadWorkerData *dt, int i, int j) {
    constexpr int num_ii = 2; // up to 3 trees per 10 meters
    constexpr int num_jj = num_ii;

    constexpr float incr = 10.f / ((float) num_ii);
    constexpr float half_incr = incr / 2.f;

    constexpr float threshold = 0.25f;
    const int seed = int(dt->nz->get_noize(float(i) + dt->pos.x_, float(j) + dt->pos.y_) * 1000.f);
    std::mt19937 mt (seed);

    for (int ii = 0; ii < num_ii; ++ii) {
        for (int jj = 0; jj < num_jj; ++jj) {

            std::uniform_real_distribution<float> dist (-half_incr, half_incr);
            const float x_offset = i + ii * incr + dist(mt);
            const float actual_x = x_offset + dt->pos.x_;

            const float y_offset = j + jj * incr + dist(mt);
            const float actual_y = y_offset + dt->pos.y_;

            std::uniform_real_distribution<float> scale_dist (0.5f, 1.5f);
            const float scale = scale_dist(mt);

            float forest_noise = dt->nz->get_forest_noize(actual_x, actual_y);
            if (forest_noise * scale_dist(mt) < threshold) {
                continue;
            }

            std::uniform_real_distribution<float> scale_xy_dist (0.85f, 1.15f);
            const float scale_side1 = scale_xy_dist(mt);
            const float scale_side2 = scale_xy_dist(mt);

            std::uniform_real_distribution<float> rot_dist (0.f, 345.f);

            const float rot = rot_dist(mt);

            const Quaternion rott = Quaternion(rot, Vector3(0, 1, 0));

            const float z_coord = dt->nz->get_noize(actual_x, actual_y) + 1;

            Matrix3x4 trans;

            trans.SetTranslation(Vector3(x_offset, z_coord, y_offset));
            trans.SetScale(Vector3(scale_side1, scale, scale_side2));
            trans.SetRotation(rott.RotationMatrix());
            dt->trees.push_back(trans);
        }
    }
}

void landscape_load_trees (ModelLoadWorkerData *dt) {
    const int init_i = round_up(dt->pos.x_, 10) - dt->pos.x_;
    const int init_j = round_up(dt->pos.y_, 10) - dt->pos.y_;

    for (int i = init_i; i < dt->size; i += 10) {
        for(int j = init_j; j < dt->size; j += 10) {
            // for every 10x10 meters piece
            landscape_load_trees_part(dt, i, j);
        }
    }
}

void landscape_load_piece (const WorkItem* item, unsigned /*threadIndex*/) {
    ModelLoadWorkerData *dt = static_cast<ModelLoadWorkerData*>(item->start_);
    if (dt->cancel) { // cancel job, cleaned up in
        return;          // Landscape::processWorkItemCompleted
    }

    dt->bounds0 = { 0.f, 0.f, 0.f };
    dt->bounds1 =
        { static_cast<float>(dt->size), 0.f, static_cast<float>(dt->size) };
    dt->position =
        { static_cast<float>(dt->pos.x_), 0.0f, static_cast<float>(dt->pos.y_) };

    create_lod_0_plane(*(dt->nz),
                       (unsigned char *const)dt->lod0.vertex_data.get()->data(),
                       (unsigned char *const)dt->lod0.index_data.get()->data(),
                       dt->bounds0.y_,
                       dt->bounds1.y_,
                       static_cast<float>(dt->size)/dt->resolution,
                       dt->resolution,
                       dt->resolution,
                       dt->pos);

    if (dt->resolution == dt->size) {
        create_lod_1_plane(*(dt->nz),
                           dt->lod1.vertex_data.get(),
                           dt->lod1.index_data.get(),
                           dt->size,
                           dt->size,
                           dt->pos);
        landscape_load_physics(dt);
        landscape_load_trees(dt);
    }
}



inline int get_closest_multiple (int num, int multiple_of) {
    // https://stackoverflow.com/a/44110705
    const int mod = num % multiple_of;
    int round = num - mod;
    if (mod > 0) {
        round += multiple_of;
    }
    return round;
}


Landscape::Landscape (Urho3D::Context *pContext)
    : Urho3D::Object(pContext) {
}

void Landscape::init (Settings *pSettings,
                      Node *pCameraNode) {
    _cfg = pSettings;
    _cameraNode = pCameraNode;
    SubscribeToEvent(E_UPDATE, URHO3D_HANDLER(Landscape, processUpdateEvent));

    debugDrawSettingUpdated(); // sets material debug draw to sync with config

    SubscribeToEvent(E_WORKITEMCOMPLETED,
                     URHO3D_HANDLER(Landscape, processWorkItemCompleted));

    _dMapRoot = GetSubsystem<UI>()->GetRoot()->CreateChild<BorderImage>();

    initSettings();

    _dMapRoot->SetSize(300, 300);
    _dMapRoot->SetHorizontalAlignment(HA_LEFT);
    _dMapRoot->SetVerticalAlignment(VA_BOTTOM);
    _dMapRoot->SetOpacity(0.6f);
    _dMapRoot->SetColor(Color::BLACK);
}



void Landscape::initSettings () {
    auto listener_func = std::bind(&Landscape::landscapeSettingUpdated, this);
    auto it = _cfg->getCategorySettingsIterator("lscp");
    while (it.isElement()) {
        it.getCurrent().addUpdateHandler(this, listener_func);

        it.peekNext();
    }

    _cfg->getSetting("debug_draw")
        .addUpdateHandler(this,
                          std::bind(&Landscape::debugDrawSettingUpdated, this));

    _cfg->getSetting("lscp.debug_map")
        .addUpdateHandler(this,
                          std::bind(&Landscape::showDbgMapSettingUpdated, this));


    landscapeSettingUpdated();
    showDbgMapSettingUpdated();
}

void validate_setting_to_be_odd_int (Setting& set) {
    const int num = set.getInt();
    if ((num % 2) != 1) {
        set.setInt(num - 1);
        URHO3D_LOGWARNING("tile size must be odd number, resetting to "
                          + Urho3D::String(num - 1));
    }
}

void Landscape::landscapeSettingUpdated () {
    validate_setting_to_be_odd_int(_cfg->getSetting("lscp.tile_size"));
    for (auto &el : _mapTiles) {
        _cameraNode->GetScene()->RemoveChild(el.second.node);
    }
    for (auto &el : _outerMapTiles) {
        _cameraNode->GetScene()->RemoveChild(el.second);
    }

    for (auto &el : _currentWorks) {
        el.second.cancel = true;
    }

    _mapTileSize = unsigned(_cfg->getSetting("lscp.tile_size").getInt());
    _outerMapTileSize = _mapTileSize * 2 + 1;

    auto num_outer_tiles =
        _cfg->getSetting("lscp.outer_tiles_num").getInt();
    auto num_tiles = _cfg->getSetting("lscp.tiles_num").getInt();
    _landscapeManager.setNumTilesToRender(size_t(num_tiles));

    _outerLandscapeManager.setNumTilesToRender(size_t(num_tiles) + size_t(num_outer_tiles));
    _outerLandscapeManager.setNumExcludedTilesToRender(size_t(num_tiles) / 2 - 1);

    const std::vector<std::tuple<FastNoise*, std::string, float*, float*>> noizes_to_process 
        = { { &_nz._noize1, "lscp.noize1", &_nz._ampl1, &_nz._exp1 },
            { &_nz._noize2, "lscp.noize2", &_nz._ampl2, &_nz._exp2 },
            { &_nz._noize3, "lscp.noize3", &_nz._ampl3, &_nz._exp3 },
            { &_nz.color1, "lscp.noize_color1", nullptr, nullptr },
            { &_nz.color2, "lscp.noize_color2", nullptr, nullptr },
            { &_nz.color3, "lscp.noize_color3", nullptr, nullptr },
    };

    for (auto el : noizes_to_process) {
        auto set_prefix = std::get<1>(el);
        auto noize = std::get<0>(el);
        auto ampl = std::get<2>(el);
        auto exp_ = std::get<3>(el);
        noize->SetFrequency(_cfg->get<float>(set_prefix + "_freq"));
        const auto t = static_cast<FastNoise::NoiseType>(_cfg->getSetting(set_prefix + "_type").getStringPos());
        noize->SetNoiseType(t);

        const auto interp = static_cast<FastNoise::Interp>(_cfg->getSetting(set_prefix + "_interp").getStringPos());
        noize->SetInterp(interp);

        const auto frac_type = static_cast<FastNoise::FractalType>(_cfg->getSetting(set_prefix + "_frac_type").getStringPos());
        noize->SetFractalType(frac_type);

        noize->SetFractalOctaves(_cfg->get<int>(set_prefix + "_frac_octaves"));
        noize->SetFractalLacunarity(_cfg->get<float>(set_prefix + "_frac_lacunarity"));
        noize->SetFractalGain(_cfg->get<float>(set_prefix + "_frac_gain"));

        exp_ && (*exp_ = float(_cfg->get<int>(set_prefix + "_exponent")));

        ampl && (*ampl = _cfg->get<float>(set_prefix + "_ampl"));
    }

    _nz._color_noise_amt = _cfg->get<float>("lscp.color_noise_amt");
    _nz._color_dither_amt = _cfg->get<float>("lscp.color_dither_amt");

    _landscapeManager.reinit();
    _outerLandscapeManager.reinit();

    _dMapRoot->RemoveAllChildren();
    _dbgMapElements.clear();
    _dbgOuterMapElements.clear();
    _mapTiles.clear();
    _outerMapTiles.clear();

    start_load_time = GetSubsystem<Time>()->GetElapsedTime();
    first_load_time = 0.f;

    _waitingToFinishTasksBeforeRestart = true;
}

void Landscape::showDbgMapSettingUpdated () {
    _showDbgMap = _cfg->get<bool>("lscp.debug_map");
    _dMapRoot->SetVisible(_showDbgMap);
}


void Landscape::debugDrawSettingUpdated () {
    Urho3D::FillMode mode =
        _cfg->getSetting("debug_draw").getBool() ? FILL_WIREFRAME : FILL_SOLID;

    GetSubsystem<ResourceCache>()
        ->GetResource<Material>("Landscape/Landscape.xml")->SetFillMode(mode);

    for (auto it = _mapTiles.cbegin(); it != _mapTiles.cend(); ++it) {
        auto node_i = (*it).second.node;
        if (_cfg->getSetting("debug_draw").getBool()) {
            if (!node_i->GetChildren().Size()) {
                displayTextOnNode(node_i, node_i->GetName());
            }
        } else {
            node_i->RemoveChild(node_i->GetChild("dbg_name"));
        }
    }
}

void Landscape::displayTextOnNode (Node *pNode,
                        const String &pText) {
    auto text_node = pNode->CreateChild("dbg_name");
    text_node->SetPosition(Vector3(0.5, 2.5, 0.5));
    auto text = text_node->CreateComponent<Text3D>();
    text->SetText(pText);
    text->SetFont(GetSubsystem<ResourceCache>()
                  ->GetResource<Font>("Fonts/Anonymous Pro.ttf"), 15);
    text->SetFaceCameraMode(FC_ROTATE_XYZ);
    text->SetFixedScreenSize(true);
}

// IntVector2 Landscape::getCameraTilewisePosition (const Vector3 &pPos) {
//     return { get_closest_multiple((int)pPos.x_ - int(_mapTileSize),
//                                   int(_mapTileSize)),
//              get_closest_multiple((int)pPos.z_ - int(_mapTileSize),
//                                   int(_mapTileSize)) };
// }

bool Landscape::workIsInProgress(const IntVector2 &pTilePos,
                                 const unsigned pResolution) {
    std::pair<decltype(_currentWorks)::iterator,
		decltype(_currentWorks)::iterator>els = _currentWorks.equal_range(pTilePos);
    for (auto it = els.first; it != els.second; ++it) {
        if (it->first == pTilePos
            && it->second.resolution == pResolution
            && it->second.cancel == false ) {
            return true;
        }
    }
    return false;
}

bool Landscape::isUpdateNeeded () {
    return _landscapeManager.getAllElementsToLoad().size() != 0
        || _landscapeManager.getAllElementsToUnload().size() != 0
        || _outerLandscapeManager.getAllElementsToLoad().size() != 0
        || _outerLandscapeManager.getAllElementsToUnload().size() != 0;
}


void Landscape::processUpdateEvent (StringHash /*eventType*/, VariantMap& /*eventData*/) {
    URHO3D_PROFILE(LandscapeTilesUpdate);

    if (_waitingToFinishTasksBeforeRestart) {
        if (_currentWorks.size()) {
            return;
        } else {
            _waitingToFinishTasksBeforeRestart = false;
        }
    }

    const IntVector2 old_cam_pos = _currentCamPos;

    _landscapeManager.setCurrentCameraTransform(
        _cameraNode->GetPosition() / (float) _mapTileSize,
        Vector3());

    _outerLandscapeManager.setCurrentCameraTransform(
        _cameraNode->GetPosition() / (float) _outerMapTileSize,
        Vector3());


    if (isUpdateNeeded()) {
        removeTilesOutsideRange();
        updateCurrentWorksTiles();
        startJobsForNeededTiles();
    } else {
        if (first_load_time == 0.f) {
            first_load_time = GetSubsystem<Time>()->GetElapsedTime()
                - start_load_time;
            URHO3D_LOGINFO("Load time: " + String(first_load_time) + "\n");
        }
    }
}

void Landscape::removeTilesOutsideRange () {
    // clean up the map frum the positions that are too far away
    auto to_unload = _landscapeManager.getAllElementsToUnload();

    for (auto el : to_unload) {
        _cameraNode->GetScene()->RemoveChild(_mapTiles[el * _mapTileSize].node);
        _mapTiles.erase(el * _mapTileSize);
    }

    _landscapeManager.clearTilesToUnload();

    // clean up the map frum the positions that are too far away
    auto outer_to_unload = _outerLandscapeManager.getAllElementsToUnload();

    for (auto el : outer_to_unload) {
        _cameraNode->GetScene()->RemoveChild(_outerMapTiles[el * _outerMapTileSize]);
        _outerMapTiles.erase(el * _outerMapTileSize);
    }

    _outerLandscapeManager.clearTilesToUnload();
}

void Landscape::updateCurrentWorksTiles () {
    for (auto& el : _currentWorks) {
        if (el.second.size == _mapTileSize) {
            // cancel all the works that are outside range
            auto opacity = 0.35f;
            if (!_landscapeManager.getTileIsNeeded(el.second.pos / _mapTileSize)) {
                el.second.cancel = true;
            }
        } else if (el.second.size == _outerMapTileSize) {
            if (!_outerLandscapeManager.getTileIsNeeded(
                    el.second.pos / _outerMapTileSize)) {
                el.second.cancel = true;
            }
        }
    }
}

constexpr size_t _tasks_to_keep = 6;
constexpr size_t _tasks_to_fire_up = 1;

void Landscape::startJobsForNeededTiles () {
    const auto tiles = _landscapeManager.getAllElementsToLoad();
    size_t sent_to_load = 0;
    // TODO add LandscapManager::getNextElementToLoad() and use that
    for (auto el : tiles) {
        auto pos = el * _mapTileSize;
        if (_currentWorks.find(pos) != _currentWorks.end()) {
            continue;
        }
        // fire up the jobs

        if (_currentWorks.size() < _tasks_to_keep) {
            createWorkItem(pos, _mapTileSize, _mapTileSize, 0);
        } else {
            break;
        }
        if (sent_to_load > _tasks_to_fire_up) {
            break;
        }
        ++sent_to_load;
    }


    const auto outer_tiles = _outerLandscapeManager.getAllElementsToLoad();
    // TODO add LandscapManager::getNextElementToLoad() and use that
    for (auto el : outer_tiles) {
        auto pos = el * _outerMapTileSize;
        if (_currentWorks.find(pos) != _currentWorks.end()) {
            continue;
        }
        // fire up the jobs

        if (_currentWorks.size() < _tasks_to_keep) {
            createWorkItem(pos, _outerMapTileSize, _mapTileSize, 0);
        } else {
            break;
        }
        if (sent_to_load > _tasks_to_fire_up) {
            break;
        }
        ++sent_to_load;
    }

}


void Landscape::createWorkItem (const Urho3D::IntVector2& pos_i,
                                const unsigned pSize,
                                const unsigned pResolution,
                                const unsigned priority) {
    auto &curr_work =
        (*_currentWorks.emplace(std::make_pair(pos_i, ModelLoadWorkerData()))).second;
    curr_work.nz = &_nz;
    curr_work.pos = pos_i;
    curr_work.size = pSize;
    curr_work.resolution = pResolution;
    curr_work.lod0.index_data =
        _cacheLod0.getFreeIndexVector(pResolution, pResolution);
    curr_work.lod0.vertex_data =
        _cacheLod0.getFreeVertexVector(pResolution, pResolution);

    curr_work.lod1.index_data =
        _cacheLod0.getFreeIndexVector(pResolution, pResolution);
    curr_work.lod1.vertex_data =
        _cacheLod0.getFreeVertexVector(pResolution, pResolution);


    SharedPtr<WorkItem> itm(new WorkItem());

    itm->sendEvent_ = true;
    itm->workFunction_ = landscape_load_piece;
    itm->start_ = &curr_work;
    itm->aux_ = context_;
    itm->priority_ = priority;

    GetSubsystem<WorkQueue>()->AddWorkItem(itm);
}

void Landscape::loadTile (ModelLoadWorkerData* dt) {
    auto model =
        create_2lod_plane(context_, dt->lod0, dt->lod1, dt->bounds0, dt->bounds1);

    _mapTiles[dt->pos].node = _cameraNode->GetScene()->CreateChild(String(dt->pos));
    auto &node = _mapTiles[dt->pos].node;

    node->SetPosition(dt->position);

    auto object = node->CreateComponent<StaticModel>();
    object->SetModel(model);

    if (_cfg->getSetting("debug_draw").getBool()) {
        displayTextOnNode(node, String(dt->pos));
    }

    object->SetMaterial(GetSubsystem<ResourceCache>()
                        ->GetResource<Material>("Landscape/Landscape.xml"));
    RigidBody* body = node->CreateComponent<RigidBody>();

    body->GetBody()->setCollisionShape(dt->physics_data->bt_shape.get());
    _mapTiles[dt->pos].physics = std::move(dt->physics_data);


    if (!_cfg->get<bool>("dbg.notrees")) {
        if (false) {
            // Group optimization doesn't seem to bring much benefi eh
            Node* treesNode = node->CreateChild("TreeGroup");
            auto treesGroup = treesNode->CreateComponent<StaticModelGroup>();
            auto crownsGroup = treesNode->CreateComponent<StaticModelGroup>();

            auto tree = buildTree(treesNode);
            treesGroup->SetModel(std::get<0>(tree));
            crownsGroup->SetModel(std::get<1>(tree));
            crownsGroup->SetMaterial(std::get<2>(tree));

            for (auto &el : dt->trees) {
                Node* aNode = node->CreateChild("Tree");
                aNode->SetTransform(el);

                //boxNodes_.Push(SharedPtr<Node>(boxNode));
                treesGroup->AddInstanceNode(aNode);
                crownsGroup->AddInstanceNode(aNode);
            }
        } else {
            auto tree = buildTree(node);

            for (auto& el : dt->trees) {
                auto aNode = node->CreateChild("Tree");
                aNode->SetTransform(el);
                auto tree_mdl = aNode->CreateComponent<StaticModel>();
                tree_mdl->SetModel(std::get<0>(tree));
                tree_mdl->SetMaterial(std::get<2>(tree));
                auto crown_mdl = aNode->CreateComponent<StaticModel>();
                crown_mdl->SetModel(std::get<1>(tree));
                crown_mdl->SetMaterial(std::get<3>(tree));

                if (!_tree_imp) {
                    _tree_imp = aNode->CreateComponent<Impostor>();
                    _tree_imp->GetMaterial()->SetShaderParameter(
                        "AmbientColor",
                        std::get<3>(tree)->GetShaderParameter("AmbientColor"));
                    _tree_imp->GetMaterial()->SetShaderParameter(
                        "MatSpecColor",
                        std::get<3>(tree)->GetShaderParameter("MatSpecColor"));
                }
                else {
                    aNode->AddComponent(_tree_imp->Clone(), 0, REPLICATED);
                }
            }
        }
    }
}

void Landscape::loadOuterTile (const ModelLoadWorkerData* dt) {
    auto model = create_1lod_plane(context_,
                                   dt->lod0,
                                   dt->bounds0,
                                   dt->bounds1);

    _outerMapTiles[dt->pos] =
        _cameraNode->GetScene()->CreateChild(String(dt->pos));
    auto &node = _outerMapTiles[dt->pos];

    node->SetPosition(dt->position);

    auto object = node->CreateComponent<StaticModel>();
    object->SetModel(model);

    if (_cfg->getSetting("debug_draw").getBool()) {
        displayTextOnNode(node, String(dt->pos));
    }

    object->SetMaterial(GetSubsystem<ResourceCache>()
                        ->GetResource<Material>("Landscape/Landscape.xml"));
}


void Landscape::processWorkItemCompleted (StringHash /*eventType*/,
                                          VariantMap& eventData) {
    WorkItem *item =
        static_cast<WorkItem*>(eventData[WorkItemCompleted::P_ITEM].GetPtr());
    ModelLoadWorkerData *dt = static_cast<ModelLoadWorkerData*>(item->start_);

    if (!dt->cancel &&
        (_landscapeManager.getTileIsNeeded(dt->pos / _mapTileSize)
         || _outerLandscapeManager.getTileIsNeeded(
                dt->pos / _outerMapTileSize))) {
        if (dt->size == _mapTileSize) {
            loadTile(dt);
            _landscapeManager.setTileIsLoaded(dt->pos / _mapTileSize);
            ++_numInnerTilesLoadedCounter;
        } else if (dt->size == _outerMapTileSize) {
            loadOuterTile(dt);
            _outerLandscapeManager.setTileIsLoaded(dt->pos / _outerMapTileSize);
            ++_numOuterTilesLoadedCounter;
        }
    }

    _cacheLod0.returnIndexVectorToCache(dt->lod0.index_data);
    _cacheLod0.returnVertexVectorToCache(dt->lod0.vertex_data);

    eraseCurrentWork(dt);
}

void Landscape::eraseCurrentWork (ModelLoadWorkerData *dt) {
    auto els = _currentWorks.equal_range(dt->pos);
    for (auto it = els.first; it != els.second; ++it) {
        if (&it->second == dt) {
            _currentWorks.erase(it);
            return;
        }
    }
}
