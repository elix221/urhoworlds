#pragma once


#include <Urho3D/Core/Object.h>


#include "Settings/Settings.hpp"


namespace Urho3D {
    class Context;
    class Node;
}


class CameraController : public Urho3D::Object {
    URHO3D_OBJECT(CameraController, Urho3D::Object);
public:
    CameraController (Urho3D::Context *pContext);
    void init (Urho3D::Node *pNode, Settings *pSettings);
    void activate ();
    void stop ();

    void cameraMoveSpeedSettingUpdated ();

private:
    void moveCamera (float pTimeStep);
    void handleUpdate (Urho3D::StringHash /*eventType*/,
                       Urho3D::VariantMap& eventData);

    Urho3D::Node *_cameraNode = nullptr;
    Settings* _settings = nullptr;

    /// Camera yaw angle.
    float yaw_ = 0.f;
    /// Camera pitch angle.
    float pitch_ = 0.f;

    float _moveSpeed = 1.f;

    bool active = false;
};
