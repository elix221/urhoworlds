// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com
#include "UrhoWorlds.hpp"


#include <Urho3D/Engine/Console.h>
#include <Urho3D/AngelScript/Script.h>
#include <Urho3D/AngelScript/ScriptFile.h>
#include <Urho3D/AngelScript/ScriptInstance.h>
#include <Urho3D/Core/CoreEvents.h>
#include <Urho3D/Core/ProcessUtils.h>
#include <Urho3D/Core/StringUtils.h>
#include <Urho3D/DebugNew.h>
#include <Urho3D/Engine/Engine.h>
#include <Urho3D/Engine/EngineEvents.h>
#include <Urho3D/Graphics/Camera.h>
#include <Urho3D/Graphics/Graphics.h>
#include <Urho3D/Graphics/Material.h>
#include <Urho3D/Graphics/Model.h>
#include <Urho3D/Graphics/Octree.h>
#include <Urho3D/Graphics/Renderer.h>
#include <Urho3D/Graphics/RenderPath.h>
#include <Urho3D/Graphics/StaticModel.h>
#include <Urho3D/Graphics/GraphicsEvents.h>
#include <Urho3D/Graphics/Zone.h>
#include <Urho3D/Input/Input.h>
#include <Urho3D/Resource/ResourceCache.h>
#include <Urho3D/Scene/Scene.h>
#include <Urho3D/UI/Font.h>
#include <Urho3D/UI/Text.h>
#include <Urho3D/UI/UI.h>
#include <Urho3D/UI/UI.h>
#include <Urho3D/UI/Text3D.h>
#include <Urho3D/Graphics/DebugRenderer.h>
#include <Urho3D/Core/WorkQueue.h>
#include <Urho3D/Urho2D/Drawable2D.h>


#include "LandscapeGenerator.hpp"
#include "TreeGenerator.hpp"
#include "SettingsGui.hpp"
#include "Impostor.hpp"

#include <cmath>
#include <algorithm>
#include <vector>
#include <deque>
#include <random>

#undef min
#include <limits>

URHO3D_DEFINE_APPLICATION_MAIN(UrhoWorlds)

// function hex_corner(center, size, i) {
//     var angle_deg = 60 * i   + 30;
//     var angle_rad = PI / 180 * angle_deg;
//     return Point(center.x + size * cos(angle_rad), center.y + size * sin(angle_rad));
// }


UrhoWorlds::UrhoWorlds (Context* context) 
    : Sample(context),
      _cfg("config.ini"),
      _console(context),
      _frameTimeGraph(context),
      _mainMenu (context, &_stateManager),
      _consoleState (context, &_stateManager),
    _appSettingsState (context, &_stateManager),
    _gameplayState (context, &_stateManager) {
    context_->RegisterSubsystem(new Script(context_));
    context_->RegisterSubsystem(new WorkQueue(context_));
    Impostor::RegisterObject(context);
    GetSubsystem<WorkQueue>()->CreateThreads(3);
    ProcSky::RegisterObject(context);
}

void UrhoWorlds::Start () {
    Sample::Start();

    initSettings ();

    createScene();

    createInstructions();

    setupViewport();

    subscribeToEvents();

    //    Sample::InitMouseMode(MM_RELATIVE);

    _console.setSettingsPtr(&_cfg);
    _console.setCameraNodePtr(cameraNode_);
    _console.start();

    _appSettingsState.init(&_cfg);
    _gameplayState.init(&_cfg, cameraNode_);
    _mainMenu.addMenuElement("Play", &_gameplayState);
    _mainMenu.addMenuElement("Settings", &_appSettingsState);
    _stateManager.pushState(&_mainMenu);


    _cfg.getSetting("dbg.ft_chart")
        .addUpdateHandler(this,
                          std::bind(&UrhoWorlds::debugFrameTimeChartSettingUpdated,
                                    this));

    _frameTimeGraph.init(_orthoCameraNode, _cfg.getSetting("dbg.ft_chart").getBool());

    if (!_cfg.get<bool>("dbg.nosky")) {
        auto sky = scene_->CreateComponent<ProcSky>();
        sky->Initialize();
    }

    _fpsUiElement = GetSubsystem<UI>()->GetRoot()->CreateChild<Text>();
    _fpsUiElement->SetFont(GetSubsystem<ResourceCache>()->GetResource<Font>("Fonts/Anonymous Pro.ttf"), 15);

    _fpsUiElement->SetHorizontalAlignment(HA_LEFT);
    _fpsUiElement->SetVerticalAlignment(VA_TOP);
    _fpsUiElement->SetPosition(0, 0);
}

void UrhoWorlds::debugFrameTimeChartSettingUpdated () {
    _frameTimeGraph.setEnabled(_cfg.getSetting("dbg.ft_chart").getBool());
}


void UrhoWorlds::createScene () {
    scene_ = new Scene(context_);

    scene_->CreateComponent<Octree>();
    auto physics_world = scene_->CreateComponent<PhysicsWorld>();
    physics_world->SetFps(240);
    //scene_->CreateComponent<DebugRenderer>();


    Node* lightNode = scene_->CreateChild("DirectionalLight");
    lightNode->SetDirection(Vector3(1.f, -0.3f, 0.f)); // The direction vector does not need to be normalized

    Light* light = lightNode->CreateComponent<Light>();
    light->SetLightType(LIGHT_DIRECTIONAL);


    cameraNode_ = scene_->CreateChild("Camera");
    cameraNode_->CreateComponent<Camera>();

    cameraNode_->SetPosition(Vector3(0.0f, 3, 0));

    auto zone = scene_->GetOrCreateComponent<Zone>();
    const auto maxfloat = std::numeric_limits<float>::max();
    const auto minfloat = std::numeric_limits<float>::min();
    zone->SetBoundingBox(BoundingBox(-maxfloat, maxfloat));
    zone->SetFogStart(maxfloat);
    zone->SetFogEnd(maxfloat);

    cameraNode_->GetComponent<Camera>()->SetFarClip(40000.f);

    // second viewport
    Renderer* renderer = GetSubsystem<Renderer>();
    ResourceCache* cache = GetSubsystem<ResourceCache>();
    cache->SetAutoReloadResources(true);

    auto scene2 = new Scene(context_);
    scene2->CreateComponent<Octree>();
    scene2->CreateComponent<DebugRenderer>();
    _orthoCameraNode = scene2->CreateChild("cameraNode");

    auto camera2 = _orthoCameraNode->CreateComponent<Camera>();
    cameraNode_->SetPosition(Vector3(0.0f, 0.0f, -10.0f));

    camera2->SetOrthographic(true);
    camera2->SetProjectionOffset(Vector2(-0.5f, -0.5f));

    auto* graphics = GetSubsystem<Graphics>();
    adjustOverlayLayerScreenSize();
    URHO3D_LOGINFO("Ortho camera projection offset: " + String(camera2->GetProjectionOffset()));

    SubscribeToEvent(GetSubsystem<Graphics>(),
                     E_SCREENMODE,
                     URHO3D_HANDLER(UrhoWorlds, handleScreenResize));

    auto viewport2 = new Viewport(context_, scene2, camera2);

    auto overlayRenderPath = new RenderPath();
    overlayRenderPath->Load(cache->GetResource<XMLFile>("RenderPaths/ForwardOverlay.xml"));
    viewport2->SetRenderPath(overlayRenderPath);

    renderer->SetNumViewports(2);
    renderer->SetViewport(1, viewport2);
}

void UrhoWorlds::createInstructions () {
    // ResourceCache* cache = GetSubsystem<ResourceCache>();
    // UI* ui = GetSubsystem<UI>();

    // Text* instructionText = ui->GetRoot()->CreateChild<Text>();
    // instructionText->SetName("StatusLine");
    // instructionText->SetText("Use WASD keys and mouse/touch to move");
    // instructionText->SetFont(cache->GetResource<Font>("Fonts/Anonymous Pro.ttf"), 15);

    // instructionText->SetHorizontalAlignment(HA_CENTER);
    // instructionText->SetVerticalAlignment(VA_BOTTOM);
    // instructionText->SetPosition(0, 0);
}


void UrhoWorlds::setupViewport () {
    Renderer* renderer = GetSubsystem<Renderer>();

    SharedPtr<Viewport> viewport(new Viewport(context_, scene_, cameraNode_->GetComponent<Camera>()));
    renderer->SetViewport(0, viewport);
}


void UrhoWorlds::subscribeToEvents () {
  SubscribeToEvent(E_UPDATE, URHO3D_HANDLER(UrhoWorlds, handleUpdate));
  SubscribeToEvent(E_KEYDOWN, URHO3D_HANDLER(UrhoWorlds, handleKeyDown));
}

void UrhoWorlds::handleKeyDown (StringHash tp, VariantMap& eventData) {
    int key = eventData[KeyUp::P_KEY].GetInt();

    if (key == KEY_F1) {
        if (_stateManager.currentState() != &_consoleState) {
            _stateManager.pushState(&_consoleState);
        }
    }

    Sample::HandleKeyDown(tp, eventData);
}

void UrhoWorlds::handleUpdate (StringHash /*eventType*/, VariantMap& eventData) {
    if (!_stateManager.update()) {
        engine_->Exit();
    }

    _fpsCounter += 1;
    _fpsTimer += eventData[Update::P_TIMESTEP].GetFloat();
    if (_fpsTimer >= 0.5f) {
        _fpsUiElement->SetText(String(((float)_fpsCounter) / _fpsTimer));
        _fpsTimer = 0.0f;
        _fpsCounter = 0;
    }

}



void UrhoWorlds::initSettings () {
    _cfg.appendSetting(Setting("lscp.tile_size", 5, 255, 129))
        .setLongName("Landscape tile size");


    _cfg.appendSetting(Setting("lscp.tiles_num", 3, 50, 5))
        .setLongName("Landscape number of tiles around to render");

    _cfg.appendSetting(Setting("lscp.outer_tiles_num", 3, 50, 10))
        .setLongName("Landscape number of far-away tiles around to render");

    _cfg.appendSetting(Setting("debug_draw", static_cast<bool>(false)))
        .setLongName("World debug draw");

    _cfg.appendSetting(Setting("dbg.ft_chart", static_cast<bool>(false)))
        .setLongName("Draw frame times chart");


    _cfg.appendSetting(Setting("_camera_move_speed", 1.f, 255.f, 20.f))
        .setLongName("Camera move speed");

    _cfg.appendSetting(Setting("lscp.debug_map", static_cast<bool>(false)))
        .setLongName("Draw debug map for landscape");

    _cfg.appendSetting(Setting("_tree.selected_preset", std::string()))
        .setLongName("Tree preset");

    _cfg.appendSetting(Setting("dbg.nolscp", static_cast<bool>(false)))
        .setLongName("Do not load landscape");

    _cfg.appendSetting(Setting("dbg.nosky", static_cast<bool>(false)))
        .setLongName("Do not load sky");

    _cfg.appendSetting(Setting("dbg.notrees", static_cast<bool>(false)))
        .setLongName("Do not load trees");

    _cfg.appendSetting(Setting("lscp.color_dither_amt", 0.f, 1.f, 0.05f));
    _cfg.appendSetting(Setting("lscp.color_noise_amt", 0.f, 1.f, 0.05f));

    for (const std::string &set_prefix : { "lscp.noize1",
                                           "lscp.noize2",
                                           "lscp.noize3",
                                           "lscp.noize_color1",
                                           "lscp.noize_color2",
                                           "lscp.noize_color3", }) {
        _cfg.appendSetting(Setting(set_prefix + "_freq", .0001f, 1.f, 0.01f))
            .setLongName("Landscape generator noize frequency");

        _cfg.appendSetting(Setting(set_prefix + "_type",
                                   { "Value",
                                     "ValueFractal",
                                     "Perlin",
                                     "PerlinFractal",
                                     "Simplex",
                                     "SimplexFractal",
                                     "Cellular",
                                     "WhiteNoise",
                                     "Cubic",
                                     "CubicFractal"},
                                   "Value"))
            .setLongName("Noise type");
        _cfg.appendSetting(Setting(set_prefix + "_interp",
                                   { "Linear",
                                     "Hermite",
                                     "Quintic"},
                                   "Hermite"))
            .setLongName("Noise interpolation type");
        _cfg.appendSetting(Setting(set_prefix + "_frac_type",
                                   { "FBM",
                                     "Billow",
                                     "RigidMulti"},
                                   "FBM"))
            .setLongName("Noise fractal type");

        _cfg.appendSetting(Setting(set_prefix + "_frac_octaves", 0, 9, 2))
            .setLongName("Noise fractal octaves");

        _cfg.appendSetting(Setting(set_prefix + "_frac_lacunarity", .01f, 100.f, 2.f))
            .setLongName("Noise fractal octaves");

        _cfg.appendSetting(Setting(set_prefix + "_frac_gain", .001f, 1.f, 0.5f))
            .setLongName("Noise fractal gain");

        if (set_prefix != "lscp.noize_color") {
            _cfg.appendSetting(Setting(set_prefix + "_exponent", 1, 10, 1))
                .setLongName("Noise exponent");

            _cfg.appendSetting(Setting(set_prefix + "_ampl", 0.f, 5000.f, 5.f))
                .setLongName("Noise amplitude");
        }
}

    _cfg.load();
}


void UrhoWorlds::adjustOverlayLayerScreenSize () {
    IntVector2 szi = GetSubsystem<Graphics>()->GetSize();
    Vector2 sz = Vector2(float(szi.x_) * PIXEL_SIZE, float(szi.y_) * PIXEL_SIZE);

    _orthoCameraNode->GetComponent<Camera>()->SetOrthoSize(sz);
}


void UrhoWorlds::handleScreenResize (StringHash /*eventType*/,
                                     VariantMap& eventData) {
    adjustOverlayLayerScreenSize();
}
