// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com
#include "MKeyFile.hpp"

#include <cassert>
#include <fstream>

#include "Utils/StringUtils.hpp"

MSettingsIterator::MSettingsIterator(MSettings::iterator b,
                                     MSettings::iterator e,
                                     MSettings& pSettings)
    : begin (b),
      end (e),
      current (b),
      settings (pSettings)
{
    this->currentIsElement = (current == end) ? false : true;
}

void MSettingsIterator::peekNext () {
    if (this->currentIsElement) {
        ++this->current;

        if (this->current == this->end) {
            this->currentIsElement = false;
        }
    }
    else {
        throw MKeyFileOutOfRangeException();
    }
}

bool MSettingsIterator::isElement () const {
    return this->currentIsElement;
}

MSettings::value_type MSettingsIterator::getSetting () const {
    if (!this->currentIsElement) {
        throw MKeyFileOutOfRangeException();
    }
    return *(this->current);
}

std::string MSettingsIterator::getName () const {
    if (!this->currentIsElement) {
        throw MKeyFileOutOfRangeException();
    }
    return this->current->first;
}

std::string MSettingsIterator::getValue () const {
    if (!this->currentIsElement) {
        throw MKeyFileOutOfRangeException();
    }
    return this->current->second;
}

std::string MSettingsIterator::findSetting (const std::string &name) const {
    if (!this->settings.count(name)) {
        throw MKeyFileSettingNotFoundException();
    }
    return this->settings[name];
}



MSectionsIterator::MSectionsIterator(MSections::iterator b,
                                     MSections::iterator e)
    : begin(b),
      end(e),
      current(b) {
    this->currentIsElement = (current == end) ? false : true;
}

void MSectionsIterator::peekNext() {
    if (this->currentIsElement) {
        ++this->current;

        if (this->current == this->end) {
            this->currentIsElement = false;
        }
    }
    else {
        throw MKeyFileOutOfRangeException();
    }
}

bool MSectionsIterator::isElement() const {
    return this->currentIsElement;
}

std::string MSectionsIterator::getName() const {
    if (!this->currentIsElement) {
        throw MKeyFileOutOfRangeException();
    }
    return this->current->first;
}

MSettingsIterator MSectionsIterator::getSettingsIterator() const {
    if (!this->currentIsElement) {
        throw MKeyFileOutOfRangeException();
    }
    return MSettingsIterator(this->current->second->begin(),
                             this->current->second->end(),
                             *(this->current->second));
}



MKeyFile::MKeyFile(const std::string& filename) {
    std::ifstream file(filename.c_str());
    if (file.bad() || file.eof() || file.fail()) {
        throw file_not_found(
            std::string("MKeyFile: failed to load ") + filename);
    }
    std::string str;
    MSettings *current_settings = new MSettings();
    std::string current_section_name("");
    this->sections.insert(
        MSections::value_type(current_section_name,
                              current_settings));

    if (file.good()) {
        while (!file.eof()) {
            std::getline(file, str);
            str = StringUtils::reduce(str);

            if (str.length() > 0 && str.at(0) != '#') {
                if (str.at(0) == '[' && str.at(str.length() - 1) == ']') {
                    current_section_name = str.substr(1, str.length() - 2);
                    current_settings = new MSettings();
                    this->sections.insert(
                        MSections::value_type(current_section_name,
                                              current_settings));
                }
                else {
                    std::string::size_type separator_pos =
                        str.find_first_of(" \n\0", 0);
                    std::string value_name = str.substr(0, separator_pos);
                    std::string value = (separator_pos + 1 == std::string::npos
                                         || separator_pos == std::string::npos) ?
                                        "" :
                                        str.substr(separator_pos + 1);
                    current_settings->insert(
                        MSettings::value_type(value_name, value));
                }
            }
        }
    }
}

MKeyFile::~MKeyFile() {
    // cleanup all sections
    MSections::iterator
        seci = this->sections.begin(),
        end = this->sections.end();

    while (seci != end) {
        // EMPROVE: rid of dynamic memory?
        delete seci->second;
        ++seci;
    }
}

MSectionsIterator MKeyFile::getSectionsIterator() {
    return MSectionsIterator(this->sections.begin(), this->sections.end());
}

void MKeyFile::appendKey (const std::string& section,
                          const std::string& key,
                          const std::string& value) {
    if (this->sections.count(section) > 1) {
        throw std::runtime_error(
            std::string("can't append key to ambiguous section ")
            + section);
    }
    MSettings *settings = NULL;
    if (this->sections.count(section) == 0) {
        settings = new MSettings();
        this->sections.insert(
            MSections::value_type(section,
                                  settings));
    }
    else {
        MSections::const_iterator it = this->sections.find(section);
        settings = (*it).second;
    }
    (*settings)[key] = value;
}

void MKeyFile::appendKey (const std::string& key, const std::string& value) {
    assert (!key.empty());
    std::string::size_type separator_pos =
        key.find_first_of(".", 0);
    const std::string section_name =
        (separator_pos + 1 == std::string::npos
         || separator_pos == std::string::npos) ?
        "" :
        key.substr(0, separator_pos);

    const std::string key_name =
        (separator_pos < std::string::npos ?
         key.substr(separator_pos + 1) :
         key);
    this->appendKey(section_name, key_name, value);

}

void MKeyFile::writeToFile (const std::string& filename) {
    std::ofstream file(filename.c_str());
    MSectionsIterator _sections = this->getSectionsIterator();
    for (;_sections.isElement(); _sections.peekNext()) {
        if (!_sections.getName().empty()) {
            file << "[" << _sections.getName() << "]" << std::endl;
        }
        MSettingsIterator settings = _sections.getSettingsIterator();
        for (;settings.isElement(); settings.peekNext()) {
            file << settings.getName() << " "
                 << settings.getValue()
                 << std::endl;
        }
    }
}
