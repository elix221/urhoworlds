#pragma once

template<typename I, typename L, typename H>
auto _clamp (const I& pVal, const L &lo, const H &hi) ->decltype(pVal+lo+hi) {
    if (pVal < lo) {
        return lo;
    } else if (pVal > hi) {
        return hi;
    }
    return pVal;
}

