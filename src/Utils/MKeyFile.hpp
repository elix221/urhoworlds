#ifndef RACINGGAME_M_KEY_FILE_HPP__
#define RACINGGAME_M_KEY_FILE_HPP__

#include <map>
#include <stdexcept>

typedef std::map<std::string, std::string> MSettings;
typedef std::multimap<std::string, MSettings *> MSections;

class MKeyFileOutOfRangeException {};
class MKeyFileSettingNotFoundException {};

class MSettingsIterator {
public:
    MSettingsIterator(MSettings::iterator b,
                      MSettings::iterator e,
                      MSettings& settings);
    void peekNext();
    bool isElement() const;
    MSettings::value_type getSetting() const;
    std::string getName() const;
    std::string getValue() const;

    std::string findSetting(const std::string &name) const;

private:
    const MSettings::iterator begin;
    const MSettings::iterator end;
    MSettings::iterator current;
    bool currentIsElement;
    MSettings& settings;
};

class MSectionsIterator {
public:
    MSectionsIterator(MSections::iterator b,
                      MSections::iterator e);
    void peekNext();
    bool isElement() const;
    std::string getName() const;
    MSettingsIterator getSettingsIterator() const;
private:
    const MSections::iterator begin;
    const MSections::iterator end;
    MSections::iterator current;
    bool currentIsElement;
};

class MKeyFile {
public:
    class file_not_found : public std::runtime_error {
    public:
        file_not_found (const std::string &message)
            : std::runtime_error (message.c_str()) {
        }
    };
    MKeyFile(const std::string& filename);
    MKeyFile() {};
    ~MKeyFile();
    MSectionsIterator getSectionsIterator();

    void appendKey (const std::string& section,
                    const std::string& key,
                    const std::string& value);
    // key format here is section.key_name or just key_name
    void appendKey (const std::string& key,
                    const std::string& value);
    void writeToFile (const std::string& filename);
private:
    MSections sections;
};

#endif /* RACINGGAME_M_KEY_FILE_HPP__ */
