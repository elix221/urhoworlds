#pragma once


#include <string>
#include <stack>

#include "IState.hpp"


class StateManager {
public:
    //StateManager ();
    //~StateManager ();

    void pushState (IState *pState);
    void popState ();
    //    void popAllStates ();

    IState *currentState () {
        return _states.top();
    }

    // To be called every frame by top-level app manager
    // returning false means stack got empty
    bool update (); 

private:
    void _doPushState ();

    std::stack<IState *> _states;
    bool _topIsFrozen = false;

    IState *_stateToPush = nullptr;
    bool _popRequested = false;
};



