#pragma once

#include <string>

class IState {
public:
    virtual ~IState () {}
    virtual void load () = 0;
    virtual void resume () {}
    virtual void freeze (IState* /*pPushedState*/) {} 
    virtual void unload () = 0;
    virtual std::string getName () const = 0;
};
