#pragma once

#include <tuple>

#include <Urho3D/Scene/Component.h>
#include <Urho3D/Scene/Node.h>
#include <Urho3D/Graphics/RenderPath.h>

namespace Urho3D
{

class Texture2D;
class RenderPath;
class Model;
class Material;
class Viewport;
class Image;

class Impostor : public Component
{
    URHO3D_OBJECT (Impostor, Component);
public:
    explicit Impostor(Context* context);
    virtual ~Impostor();

    static void RegisterObject(Context* context);

    virtual void OnMarkedDirty(Node* node) override;
    virtual void OnNodeSet(Node* node) override;

    void SetOffset (const Vector3 &offset, bool parent_space = false) {
        positionOffset_ = offset;
        positionOffsetUseParentSpace_ = parent_space;
    }

    Material* GetMaterial() {
        return material_;
    }

    Impostor* Clone();


protected:
    friend class ImpostorsCurator;
    //accessed by ImpostorsCurator;
    void SwitchActive (const bool active);

    Impostor(const Impostor &);
    Impostor& operator=(const Impostor& other) = delete;

    void CreateImpostor();
    void ReassembleModel();
    void StartImpostorRender();
    void SetCameraPosition();
    void HandleRenderDone(StringHash eventType,
                          VariantMap& eventData);

    SharedPtr<Model> CreatePlane (const float uv_incr,
                                  const Vector2 &base,
                                  const float scale);
    void SpawnDebugElement(const Vector3 &position, const float dbg_scale);

    void CleanUp ();

    void GatherImpostorPart(Texture2D *tex,
                            Image* img,
                            const Vector2 &uv_coord);

    void ExportImage(Image*, const String &name);

    void ProcessImpostorPartRendered();
    void UpdateImpostorScreen(Vector3);
    void SetupScreen();

    int impostorFrameNumber = 0;

    const float impostorMarginMultiplier = 1.4142136f;

    SharedPtr<Material> material_;
    Node* sceneCamera_ = nullptr;
    Node* screenNode_ = nullptr;
    Vector3 boundsMask_ = Vector3(1.f, 1.f, 1.f);

    bool actAsBillboard_ = true;

    Vector3 positionOffset_;
    bool positionOffsetUseParentSpace_ = false;

    Image* impResultImage_ = nullptr;
    Image* impResultDepthImage_ = nullptr;

    // All the inner stuff there:
    Scene* impScene_ =  nullptr;
    Node* impCameraNode_ = nullptr;
    Node *impObjectNode_ = nullptr;

    Texture2D* screenTexture_ = nullptr;
    Texture2D* screenTextureBump_ = nullptr;


    Texture2D* impRenderTexture_ = nullptr;
    Texture2D* impRenderTextureDepth_ = nullptr;
    Viewport* impViewport_ = nullptr;
    RenderPath* impRenderPath_ = nullptr;

    BoundingBox bounds_;
    Image* buffer;

    int distance_ = 10;

    bool amCopied_ = false;
    bool needRegen_ = true;
    SharedPtr<Model> plane_;
};

} // namespace Urho3D
