#pragma once

#include <Urho3D/Scene/Scene.h>
#include <Urho3D/Graphics/Model.h>
#include <Urho3D/Graphics/GraphicsDefs.h>
#include <Urho3D/Math/Vector2.h>
#include <Urho3D/Math/Vector3.h>
#include <vector>
#include <memory>
#include <stdexcept>

#include <FastNoise.h>


struct Hehe {
    static size_t _num_relocations;
};

template <typename T>
class UrhoworldsMemory {
public:
    explicit UrhoworldsMemory (const size_t sz = 0) {
        if (sz)
            _data = new T[sz];
        _allocated_size = sz;
        _used_size = 0;
    }

    ~UrhoworldsMemory() {
        delete[] _data;
    }

    UrhoworldsMemory (const UrhoworldsMemory&) = delete;
    UrhoworldsMemory operator== (const UrhoworldsMemory&) = delete;
    UrhoworldsMemory (UrhoworldsMemory &&) = delete;
    UrhoworldsMemory operator== (UrhoworldsMemory &&) = delete;

    size_t size () {
        return _used_size;
    }

    void resize (const size_t pSize) {
        reserve(pSize);
        _used_size = pSize;
    }

    void reserve (const size_t pSize) {
        if (pSize > _allocated_size) {
            grow(pSize);
        }
    }

    T& operator[] (const size_t pNum) {
        if (pNum >= _used_size) {
            throw std::out_of_range("eh");
        }
        return _data[pNum];
    }

    const T& operator[] (const size_t pNum) const {
        if (pNum >= _used_size) {
            throw std::out_of_range("ehh");
        }
        return _data[pNum];
    }

    void grow (const size_t pNewSize) {
        T* new_ptr = new T[pNewSize];
        std::memcpy(new_ptr, _data, _used_size * sizeof(T));
        _allocated_size = pNewSize;
        delete[] _data;
        _data = new_ptr;

        ++Hehe::_num_relocations;
    }

    void push_back (const T& pElement) {
        if (_pos >= _allocated_size) {
            grow(std::max<size_t>(8, _allocated_size * 2));
        }
        _data[_pos++] = pElement;
        _used_size = _pos;
    }

    T* data () {
        return _data;
    }

    size_t capacity () const {
        return _allocated_size;
    }

    static size_t getNumRelocations () {
        return Hehe::_num_relocations;
    }

private:
    size_t _pos = 0;
    size_t _used_size;
    size_t _allocated_size;
    T *_data = nullptr;
};



class Noizes {
public:
    Noizes ();

    float get_noize (float x, float y) const;

    float get_color_noize1 (float x, float y) const;
    float get_color_noize2 (float x, float y) const;
    float get_color_noize3 (float x, float y) const;
    float get_forest_noize (const float x, const float y) const;

    FastNoise color1, color2, color3, forest;
    FastNoise _noize1;
    FastNoise _noize2;
    FastNoise _noize3;

    float _ampl1;
    float _ampl2;
    float _ampl3;

    float _exp1;
    float _exp2;
    float _exp3;

    float _color_noise_amt;
    float _color_dither_amt;
};


struct UrhoworldsIndexElement {
  unsigned short v1, v2, v3;
};

struct UrhoworldsColorVertexElement {
  unsigned char r, g, b, a;
};


struct UrhoworldsLandscapeVertexElement {
  Urho3D::Vector3 coord;
  Urho3D::Vector3 normal;
  UrhoworldsColorVertexElement color;
  static Urho3D::PODVector<Urho3D::VertexElement>
  getVertexDescription() {
      Urho3D::PODVector<Urho3D::VertexElement> descr;
      descr.Push(Urho3D::VertexElement(Urho3D::TYPE_VECTOR3, Urho3D::SEM_POSITION));
      descr.Push(Urho3D::VertexElement(Urho3D::TYPE_VECTOR3, Urho3D::SEM_NORMAL));
      descr.Push(Urho3D::VertexElement(Urho3D::TYPE_UBYTE4, Urho3D::SEM_COLOR));
      return descr;
  }
};

struct UrhoworldsTexturedVertexElement {
    Urho3D::Vector3 coord;
    Urho3D::Vector3 normal;
    Urho3D::Vector2 texcoord;
    static Urho3D::PODVector<Urho3D::VertexElement>
    getVertexDescription() {
        Urho3D::PODVector<Urho3D::VertexElement> descr;
        descr.Push(Urho3D::VertexElement(Urho3D::TYPE_VECTOR3, Urho3D::SEM_POSITION));
        descr.Push(Urho3D::VertexElement(Urho3D::TYPE_VECTOR3, Urho3D::SEM_NORMAL));
        descr.Push(Urho3D::VertexElement(Urho3D::TYPE_VECTOR2, Urho3D::SEM_TEXCOORD));
        return descr;
    }
};


class GeometryConstructionData {
public:
    virtual Urho3D::PODVector<Urho3D::VertexElement>
    getVertexDescription () const = 0;

    virtual void *getVertexData () const = 0;
    virtual unsigned getVertexDataSize () const = 0;

    virtual void *getIndexData () const = 0;
    virtual unsigned getIndexDataSize () const = 0;
};


template <typename VertexElementType>
class UrhoworldsPlaneConstructionData final : public GeometryConstructionData {
public:
    UrhoworldsPlaneConstructionData ()
        : index_data (std::make_unique<UrhoworldsMemory<UrhoworldsIndexElement>>()),
          vertex_data (std::make_unique<UrhoworldsMemory<VertexElementType>>()) {
    }

    Urho3D::PODVector<Urho3D::VertexElement>
    getVertexDescription () const override {
        return VertexElementType::getVertexDescription();
    }

    void *getVertexData () const override { return vertex_data->data(); }
    unsigned getVertexDataSize () const override {
        return (unsigned) vertex_data->size();
    }

    void *getIndexData () const override { return index_data->data(); }
    unsigned getIndexDataSize () const override {
        constexpr size_t sz =
            sizeof(UrhoworldsIndexElement) / sizeof(UrhoworldsIndexElement::v1);
        return (unsigned) (index_data->size() * sz);
    }


    std::unique_ptr<UrhoworldsMemory<UrhoworldsIndexElement>> index_data;
    std::unique_ptr<UrhoworldsMemory<VertexElementType>> vertex_data;
};


void add_plane (Urho3D::Scene *pScene, Urho3D::Context *pContext, int res_x, int res_y);
Urho3D::SharedPtr<Urho3D::Model> create_plane (Urho3D::Context *pContext,
                                               int res_x,
                                               int res_y,
                                               Urho3D::IntVector2 pos);

void create_lod_0_plane (const Noizes& nz,
                         unsigned char *const pVertexData,
                         unsigned char *const pIndexData,
                         float &rHeightMin,
                         float &rHeightMax,
                         float xy_scale,
                         unsigned res_x,
                         unsigned res_y,
                         Urho3D::IntVector2 pos);

void create_lod_1_plane
    (const Noizes &nz,
     UrhoworldsMemory<UrhoworldsLandscapeVertexElement> *const pVertexData,
     UrhoworldsMemory<UrhoworldsIndexElement> *const pIndexData,
     unsigned short res_x,
     unsigned short res_y,
     Urho3D::IntVector2 pos);

Urho3D::SharedPtr<Urho3D::Model>
create_2lod_plane (Urho3D::Context* pContext,
                   const GeometryConstructionData &lod0,
                   const GeometryConstructionData &lod1,
                   const Urho3D::Vector3 &bounds0,
                   const Urho3D::Vector3 &bounds1);

Urho3D::SharedPtr<Urho3D::Model>
create_1lod_plane (Urho3D::Context* pContext,
                   const GeometryConstructionData &lod0,
                   const Urho3D::Vector3 &bounds0,
                   const Urho3D::Vector3 &bounds1,
                   const Urho3D::PrimitiveType pType = Urho3D::TRIANGLE_LIST);
