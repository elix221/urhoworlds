#pragma once

#include <Urho3D/UI/UIElement.h>
#include <Urho3D/UI/Button.h>
#include <Urho3D/UI/Text.h>
#include <Urho3D/Container/Str.h>

namespace UrhoUtils {

using namespace Urho3D;

inline Button* createButton (UIElement *pParent,
                             const String &pLabelText) {
    auto button = pParent->CreateChild<Button>();
    auto label = button->CreateChild<Text>();

    button->SetMinSize(0, 24);
    button->SetVerticalAlignment(VA_TOP);
    button->SetStyleAuto();

    label->SetText(pLabelText);
    label->SetStyleAuto();
    label->SetAlignment(HA_CENTER, VA_CENTER);

    return button;
}

}
